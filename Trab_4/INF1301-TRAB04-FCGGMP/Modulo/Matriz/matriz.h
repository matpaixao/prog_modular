﻿#if !defined(MATRIZ_)
#define MATRIZ_

/**************************************************************
*
*   M�dulo de Defini��o: M�dulo Matriz
*
*   Autores: 	glg - Guilherme de Lacerda Gomes
*            	fac - Fernanda de Almeida Castro
*			 	mrp - Matheus Romero Paixão
*
*   Hist�rico de evolu��o:
*
*   Vers�o      Autor       Data        Observa��es
*   1.0.0        glg   		28/08/2019  Inicio de desenvolvimento.
*	1.0.1        glg   		29/08/2019  Adi��o das estruturas e corre��o em fun��es.
*   1.1.0        glg   		04/09/2019  Adi��o e remo��o de condi��es de retorno.
*	1.2.0		 glg   		07/09/2019  Corre��es finais.
*	1.3.0		 glg   		09/09/2019  Padroniza��o dos nomes das vari�veis.
*	1.4.0	     glg   		22/09/2019  Adi��o da fun��o ModificarTipoNo. Modifica��o da fun��o IrParaNo (incluindo tipos de movimenta��o).
*   1.4.1	     fac   		23/09/2019  Adição das Assertivas e remocão de funções inuteis
*	1.4.2		 mrp		25/09/2019	HotFixes e Regionamento 
*	------------------ Refatoração (- 0.1.0) ------------------
*	1.3.1 		 mrp		26/09/2019  Reposição de funções 
*	1.3.3		 glg		27/09/2019	Reorganização elaborada(+ 0.0.2)
*   1.3.4        fac     	29/09/2019  Assertivas corrigidas
*	1.3.5 		 mrp		30/09/2019  Hotfixes
*	1.3.6		 mrp		01/10/2019  Hotfixes 
*	1.4.0		 glg		02/10/2019  Correção da função obter valor
*	1.5.0		 glg		29/11/2019  Insercao das funcoes de deturpacao e verificacao.
*
*
*   Descri��o do M�dulo:
*       Este m�dulo visa implementar fun��es para criar, destruir, inserir,
*       e navegar por matrizes.
*       Cada matriz possui uma estrutura "cabe�a", no qual possui refer�ncia
*       para o n� (1,1), escolhido para ser o n� "principal", e para o n�
*       corrente.
*       Inicialmente, n�o h� nenhuma matriz. As matrizes podem estar vazias,
*       nesse caso, cada n� aponta para NULL.
*
***************************************************************/

#if defined(MATRIZ_OWN)
#define MATRIZ_EXT
#else
#define MATRIZ_EXT extern
#endif

typedef struct MAT_tpMatriz *MAT_tppMatriz;

typedef enum
{
	/* A��o executada corretamente */
	MAT_CondRetOK = 0,

	/* Matriz n�o existe (tam = 0) */
	MAT_CondRetMatrizNaoExiste = 1,

	/* Matriz est� vazia */
	MAT_CondRetMatrizVazia = 2,

	/* No corrente n�o possui ajac�ncia (matriz 1x1 ou extremidades) */
	MAT_CondRetNaoPossuiAdjacencia = 3,

	/* Faltou mem�ria */
	MAT_CondRetFaltouMemoria = 4,

	/* Tamanho de Matriz Inválida */
	MAT_CondRetTamanhoInvalido = 5,

	/* N� n�o possui nenhum valor (NULL). */
	MAT_CondRetValNoNull = 6

} MAT_tpCondRet;

/*************************************************************
*
*   Fun��o: MAT Criar Matriz
*
*	Par�metros
*		tam - Inteiro contendo o tamanho da
*			  matriz.
*   Descri��o: Cria uma nova matriz vazia.
*              Caso ja haja uma matriz, uma
*              nova � criada.
*
*   Valor Retornado:
*       - MAT_CondRetOK
*       - MAT_CondRetFaltouMemoria
*		- MAT_CondRetMatrizNaoExiste
*
*      Assertivas:
*      AE - Ponteiro para uma matriz
*            -Inteiro com o tamanho da matriz a ser criada
*      AS - Matriz criada
*
**************************************************************/
MAT_tpCondRet MAT_CriarMatriz(MAT_tppMatriz *pMatriz, int tam);

/*************************************************************
*
*   Fun��o: MAT Destruir Matriz
*
*   Par�metros:
*       pMatriz - Ponteiro para matriz a ser destru�da.
*		tam     - Inteiro contendo o n�mero de linhas e colunas da mtriz.
*
*   Descri��o: Destr�i uma matriz espec�fica
*              (corpo e cabe�a).
*
*   Valor Retornado:
*       - MAT_CondRetOK
*
*      Assertivas:
*      AE - Ponteiro para uma matriz
*            - Inteiro com a dimensão da matriz a ser destruída
*      AS - Matriz excluida
*
**************************************************************/
MAT_tpCondRet MAT_DestroiMatriz(MAT_tppMatriz *pMatriz, int tam);

/*************************************************************
*
*   Fun��o: MAT Esvaziar Matriz
*
*   Par�metros:
*       pMatriz - Ponteiro para matriz a ser esvaziada.
*
*		tam     - Tamanho da matriz.
*
*   Descri��o: Esvazia uma matriz espec�fica
*              (corpo e cabe�a). Ou seja, faz todos os
*			   campos "valor" dos n�s apontarem para NULL.
*
*   Valor Retornado:
*       - MAT_CondRetOK
*       - MAT_CondRetMatrizVazia
*       - MAT_CondRetMatrizNaoExiste
*
*      Assertivas:
*      AE - Ponteiro para uma matriz
*            -Inteiro com o tamanho da matriz a ser esvaziada
*      AS - Matriz vazia
*
**************************************************************/
MAT_tpCondRet MAT_EsvaziaMatriz(MAT_tppMatriz *pMatriz, int tam);

/*************************************************************
*
*   Fun��o: MAT Inserir Valor
*
*   Par�metros:
*       pMatriz - Ponteiro para matriz a qual deseja-se
*                 inserir o elemento.
*       pValor  - Ponteiro para o valor a ser inserido
*                 (pode ser NULL).
*
*   Descri��o: Insere um elemento no n� corrente da matriz
*              selecionada.
*
*   Valor Retornado:
*       - MAT_CondRetOK
*       - MAT_CondRetMatrizNaoExiste
*
*      Assertivas:
*      AE - Ponteiro para uma matriz
*            - Ponteiro para o valor a ser inserido na matriz
*      AS - Valor inserido na matriz
*
**************************************************************/
MAT_tpCondRet MAT_InsereValor(MAT_tppMatriz *pMatriz, void *pValor);

/*************************************************************
*
*   Fun��o: MAT Ir Para N�
*
*   Par�metros:
*       pMatriz - Ponteiro para matriz a qual se deseja
*                 locomover.
*       dir	    - Inteiro contendo a direcao que deseja-se
				  locomover.

*
*   Descri��o: Dada uma certa dire��o, muda-se o n�
*              corrente para o n� solicitado.
*
*   Valor Retornado:
*       - MAT_CondRetOK
*       - MAT_CondRetMatrizNaoExiste
*       - MAT_CondRetNaoPossuiAjacencia
*
*	Dire��es
*	1 - noCimaEsq
*	2 - noCimaDir
*	3 - noBaixoEsq
*	4 - noBaixoDir
*	5 - noCima
*	6 - noBaixo
*	7 - noEsq
*	8 - noDir
*
*      Assertivas:
*      AE - Ponteiro para uma matriz
*            - Inteiro com o tipo de direção que deseja-se locomover
*      AS - Inteiro com o tipo de movimentação
*
**************************************************************/
MAT_tpCondRet MAT_IrParaNo(MAT_tppMatriz *pMatriz, int dir);

/*************************************************************
*
*   Fun��o: MAT Obter Valor Corrente
*
*   Par�metros:
*       pMatriz - Ponteiro para matriz a qual deseja-se
*                 obter o valor corrente.
*       pValor  - Ponteiro para Ponteiro que receber� o valor contido
*                 no n� corrente.
*
*   Descri��o: Dada uma certa matriz, obtem-se o valor
*              contido no n� corrente apontado pelo cabe�a.
*			   Caso seja passado como par�metro uma matriz nula
*              (0x0), o valor atribu�do � pValor � NULL;
*
*   Valor Retornado:
*       - MAT_CondRetOK
*       - MAT_CondRetMatrizNaoExiste
*       - MAT_CondRetValNoNull
*
*      Assertivas:
*      AE - Ponteiro para uma matriz
*            - Ponteiro para um valor
*      AS - Valor do nó corrente da matriz
*
**************************************************************/
MAT_tpCondRet MAT_ObterValorCorr(MAT_tppMatriz *pMatriz, void **pValor);

/*************************************************************
*
*   Fun��o: MAT Deturpar
*
*   Par�metros:
*      
*
*   Descri��o: 

*   Valor Retornado:

*
*      Assertivas:

*
**************************************************************/
MAT_tpCondRet MAT_Deturpar(MAT_tppMatriz *pMatriz, int tipoDeturpacao, int direcao, int lixo);

/*************************************************************
*
*   Fun��o: MAT Verificar Matriz
*
*   Par�metros:
*      Ponteiro para pMatriz
*
*   Descri��o: 
*		Realiza a Verificação Estrutural da Matriz

*   Valor Retornado:
		Inteiro indicando numero total de erros encontrados 
*
*      Assertivas:
			AE - Ponteiro para um elemento do tipo tppMatriz
			AS - Variavel do tipo Inteiro 

*
**************************************************************/
MAT_tpCondRet MAT_VerificarMatriz(MAT_tppMatriz *pMatriz);

#undef MATRIZ_EXT

/************** Fim do m�dulo definido: M�dulo Matriz. **************/

#else
#endif
