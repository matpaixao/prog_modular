﻿
/***************************************************************************
*  $MCI M�dulo de implementa��o: M�dulo matriz
*
*  Arquivo gerado:              MATRIZ.C
*  Letras identificadoras:      MAT
*
*  Projeto: Disciplina INF1301
*  Gestor:  DI/PUC-Rio
*  Autores: glg - Gulherme de Lacerda Gomes
*           mrp - Matheus Romero Paixão
*           fac - Fernanda de Almeida Castro
*
*  $HA Historico de evolucão:	
*	Legenda :
*		HotFixes, Atualizacões e enums 		: Versão += 0.0.1 
*		Adicões e/ou remocões de funcões 	: Versão += 0.1.0
*		Finalizacão do Trabalho 			: Versão += 1.0.0
*
*     Versão  Autor    Data     Observacões
*		1.0.1    glg  31/08/2019 Início do desenvolvimento
*		1.1.0    glg  01/09/2019 Adi��o de Fun��es Auxiliares e corre��o de erros.
*		1.1.1    glg  02/09/2019 --
*		1.1.2    glg  03/09/2019 Corre��o de Bugs e in�cio do desenvolvimento de novas fun��es do m�dulo de interface.
*		1.2.0    glg  03/09/2019 Adi��o de mais fun��es do m�dulo de interface.
*       2.0.0    glg  04/09/2019 Finaliza��o da implementa��o das fun��es do m�dulo de interface e testes unit�rios do m�dulo MATRIZ.c.
*		2.1.0    glg  07/09/2019 Corre��es finais.
*		2.1.1    glg  08/09/2019 Corre��o de erros na fun��o DestruirMatriz.
*		2.2.0	 glg  09/09/2019 Padroniza��o dos nomes das vari�veis.
*		2.3.0	 glg  18/09/2019 Modifica��o do tipo noMatriz para a aplica��o do labirinto.
*		2.3.1	 mrp  22/09/2019 Modificacões no tipo Matriz e adicão de novos enums 
*		2.4.0	 glg  22/09/2019 Adi��o da fun��o ModificarTipoNo e modifica��es no tpNoMatriz. Modifica��es na fun��o de locomo��o pela matriz.
*		2.4.1	 mrp  23/09/2019 Reorganiza funcões, adiciona regions e enums
*                2.5.0   fac  23/09/2019 remocão de funcões inúteis
*		2.5.1	 mrp  25/09/2019 Adiciona Enum e Atualiza funcão 
*		------------------ Refatoracão ( - 0.3.0 ) ------------------
*		2.2.1    mrp  26/09/2019 Recolocacão e reorganizacão de Modulos
*		2.3.0	 glg  27/09/2019 Conversão de funcões e Organizacão Elaborada 
*		2.3.1	 mrp  01/10/2019 Hotfixes 
*		2.4.0    glg  02/10/2019 Correcão da funcão obter valor.
*		2.5.0    glg  29/11/2019 Inicio do desenvolvimento das funcoes Deturpar e Verificar e alteracao na estrutura (transformando em auto-verificavel).
*		2.6.0    glgl 30/11/2019 Insercao das diretivas #ifdef _DEBUG #endif.
*                2.5.1     fac  30/11/2019  correcao das estruturas para tornalas auto-verificaveis
* 
***************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#define MATRIZ_OWN
#include "matriz.h"
#undef MATRIZ_OWN
#ifdef _DEBUG
#include "CESPDIN.h"
#include "CONTA.h"
#endif

/***********************************************************************
*
*  $TC Tipo de dados: MAT Descritor da posi��o do n�
*
*
*  $ED Descri��o do tipo
*     Estas condi��es foram criadas para auxiliar na identifica��o da
*	  posi��o do n�, para facilidade a liga��o com os n�s adjacentes.
*
***********************************************************************/

typedef enum
{

	/* O n� encontra-se no canto superior esquerdo. */
	POS_CantoSupEsq = 1,

	/* O n� encontra-se no canto superior direito. */
	POS_CantoSupDir = 2,

	/* O n� encontra-se no canto inferior esquerdo.*/
	POS_CantoInfEsq = 3,

	/* O n� encontra-se no canto inferior direito. */
	POS_CantoInfDir = 4,

	/* O n� encontra-se na extremidade superior (linha mais a cima da matriz). */
	POS_ExtremidadeSup = 5,

	/* O n� encontra-se na extremidade inferior (linha mais a baixo da matriz). */
	POS_ExtremidadeInf = 6,

	/* O n� encontra-se na extremidade esquerda (linha mais a esquerda da matriz). */
	POS_ExtremidadeEsq = 7,

	/* O n� encontra-se na extremidade direita (linha mais a direita da matriz). */
	POS_ExtremidadeDir = 8,

	/* O n� encontra-se em uma posi��o gen�rica (possui oito liga��es). */
	POS_Generico = 9

} ePOS_posNo;

/***********************************************************************
*
*  $TC Tipo de dados: MAT Descritor do n� de uma matriz.
*
*
*  $ED Descri��o do tipo
*     O tipo refere-se ao n� de uma matriz. Cont�m ponteiros para os n�s adjacentes
*	  (caso n�o haja determinado n� adjacente, o ponteiro aponta para NULL) e para o
*	  valor inserido no n�. Possui, tamb�m, inteiros indicando a linha e a coluna do n�.
*
***********************************************************************/

typedef struct tpNoMatriz
{

	/* Ponteiro que aponta para o n� superior
	 *  Caso nao exista um n� superior, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoCima;

	/* Ponteiro que aponta para o n� inferior
	 *  Caso nao exista um n� inferior, o ponteiro
	 *  aponta para NULL. **/
	struct tpNoMatriz *pNoBaixo;

	/* Ponteiro que aponta para o n� a direita
	 *  Caso nao exista um n� a direita, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoDir;

	/* Ponteiro que aponta para o n� a esquerda
	 *  Caso nao exista um n� a esquerda, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoEsq;

	/* Ponteiro que aponta para o n� a noroeste
	 *  Caso nao exista um n� a noroeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoCimaEsq;

	/* Ponteiro que aponta para o n� a nordeste
	 *  Caso nao exista um n� a nordeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoCimaDir;

	/* Ponteiro que aponta para o n� a sudoeste
	 *  Caso nao exista um n� a sudoeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoBaixoEsq;

	/* Ponteiro que aponta para o n� a sudeste
	 *  Caso nao exista um n� a sudeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoBaixoDir;

	/* Ponteiro que aponta para o valor corrente
	* do n�. */
	void *pValor;

	/* Inteiro contendo a posi��o relativa a linha
	* do n�. */
	int linha;

	/* Inteiro contendo a posi��o relativa a coluna
	* do n�. */
	int coluna;
#ifdef _DEBUG
	/* Tipo do dado contido no no. */
	int tipo;

	/* tamanho do no*/
	int tam;

	/* Ponteiro que aponta para o no cabeca
     * da estrutura. Adicionado para tornar a estrutura auto-verificavel. */
	struct tpNoMatriz *pNoCab;
#endif
} tpNoMatriz;

/***********************************************************************
*
*  $TC Tipo de dados: MAT Descritor da cabe�a de uma matriz
*
*
*  $ED Descri��o do tipo
*     O tipo refere-se � cabe�a de uma matriz. Cont�m um ponteiro para o n�
*	  principal (1, 1), e um ponteiro para o n� corrente. Com isso, pode-se
*     locomover pela matriz facilmente.
*
***********************************************************************/

typedef struct MAT_tagMatriz
{
	/* Ponteiro que aponta para o n� "cabe�a", que
	*  no caso consideramos o n� (1,1). */
	tpNoMatriz *pNoCabeca;

	/* Ponteiro que aponta para o n� corrente. */
	tpNoMatriz *pNoCorrente;

#ifdef _DEBUG
	/* quantidade de nos */
	int totalNos;

	/* tamanho da estrutura (s nos)*/
	int tamNos;
#endif
} MAT_tpMatriz;

/* Prototipos das funcões encapsuladas no modulo */

static tpNoMatriz *CriaNo(int linha, int coluna);

static MAT_tpCondRet LigaNos(tpNoMatriz **listaNos, int tam, tpNoMatriz *pCab);

static ePOS_posNo EncontraPosicaoNo(tpNoMatriz tpNo, int tam);

static tpNoMatriz *ProcuraNo(tpNoMatriz **listaNo, int linha, int coluna, int tam);

static void DesencadearNo(tpNoMatriz *tpNo);

#ifdef _DEBUG

/* Funcões do Verificador */

/* Verifica se a Matriz esta apontando para um no no noCorrente  */
static int Ver_CorrenteNull(tpNoMatriz *pNoMatriz);

/* Verifica se o no de determinada direcao e null */
static int Ver_NoNull(tpNoMatriz *pNoMatriz, ePOS_posNo direcao);

/* Verifica se o no em determinada direcao e lixo */
static int Ver_NoLixo(tpNoMatriz *pNoMatriz, ePOS_posNo direcao);

/* Verifica se conteudo do no e nulo */
static int Ver_ConteudoNull(void *pValor);

/* Verifica se o no tem um tipo correto */
static int Ver_Tipo(int tipo);

/* Verifica se no corrente foi liberado sem free  */
static int Ver_LibertoSemFree(tpNoMatriz *pNoMatriz);

/* Verifica se ponteiro do no esta apontando para o cabeca  */
static int Ver_NullCabeca(tpNoMatriz *pMatriz);

#endif

/***************************************************************************
*
*  Funcão: MAT Criar Matriz
*
***************************************************************************/

MAT_tpCondRet MAT_CriarMatriz(MAT_tpMatriz *pMatriz, int tam)
{
	int linha = 1;
	int coluna = 1;
	int indice = 0;
	int step = 1;
	int counter = 0;
	MAT_tpCondRet ret;
	tpNoMatriz *pNoCorr;

	/* Vetor auxiliar para armazenar os n�s. */
	tpNoMatriz **pListaNo;

	/* Se tamanho <= 0 então o tamanho e inválido*/
	if (tam <= 0)
	{
		return MAT_CondRetTamanhoInvalido;
	}

	pListaNo = malloc(sizeof(tpNoMatriz *) * (tam * tam));
	if (pListaNo == NULL)
	{
		return MAT_CondRetFaltouMemoria;
	}

	/* Inser��o dos n�s no vetor, de acordo com o tamanho passado como par�metro. */
	for (counter = 0; counter <= (tam * tam) - 1; counter++)
	{
		if (counter % tam == 0 && counter != 0)
		{

			linha++;
			coluna = 1;
		}

		pNoCorr = CriaNo(linha, coluna);

		if (pNoCorr == NULL)
		{
			return MAT_CondRetFaltouMemoria;
		}

		if (tam == 1)
		{
			pMatriz->pNoCabeca = pNoCorr;
			pMatriz->pNoCorrente = pNoCorr;
			return MAT_CondRetOK;
		}

		if (step == 1)
		{
			pMatriz->pNoCabeca = pNoCorr;
			pMatriz->pNoCorrente = pNoCorr;
		}

		pListaNo[indice] = malloc(sizeof(tpNoMatriz));
		if (pListaNo[indice] == NULL)
		{
			return MAT_CondRetFaltouMemoria;
		}
		pListaNo[indice] = pNoCorr;

		indice++;
		step++;
		coluna++;
	}

	/* Liga��o dos n�s com seus adjacentes. */
	ret = LigaNos(pListaNo, tam, pMatriz->pNoCabeca);

	return ret;
}

/***************************************************************************
*
*  Funcão: MAT Ir Para No
*
***************************************************************************/

MAT_tpCondRet MAT_IrParaNo(MAT_tpMatriz *pMatriz, int dir)
{
	/* Matriz Nula */
	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	/* Verifica dire��o desejada. Primeiro, verifica-se se h� um n� em tal dire��o. */
	switch (dir)
	{
	case 1:
		if (pMatriz->pNoCorrente->pNoCimaEsq == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoCimaEsq;
		break;

	case 2:
		if (pMatriz->pNoCorrente->pNoCimaDir == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoCimaDir;
		break;

	case 3:
		if (pMatriz->pNoCorrente->pNoBaixoEsq == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixoEsq;
		break;

	case 4:
		if (pMatriz->pNoCorrente->pNoBaixoDir == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixoDir;
		break;

	case 5:
		if (pMatriz->pNoCorrente->pNoCima == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoCima;
		break;

	case 6:
		if (pMatriz->pNoCorrente->pNoBaixo == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixo;
		break;

	case 7:
		if (pMatriz->pNoCorrente->pNoEsq == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoEsq;
		break;

	case 8:
		if (pMatriz->pNoCorrente->pNoDir == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoDir;
		break;
	}
	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Funcão: MAT Destruir Matriz
*
***************************************************************************/

MAT_tpCondRet MAT_DestroiMatriz(MAT_tpMatriz *pMatriz, int tam)
{
	/* Vari�vel auxiliar criada para indicar
	 * qual dire��o deve-se locomover para
	 * continuar deletando (free) os n�s.
	 */
	int direcao = 1;

	int j = -1;
	int i = -1;
	tpNoMatriz *pProx = pMatriz->pNoCabeca;
	pMatriz->pNoCorrente = pMatriz->pNoCabeca;

	if (tam == 0)
	{
		return MAT_CondRetTamanhoInvalido;
	}

	for (i = 0; i < tam; i++)
	{
		switch (direcao)
		{
		case 1:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pNoDir != NULL)
				{
					pProx = pMatriz->pNoCorrente->pNoDir;
				}
				else
				{
					pProx = pProx->pNoBaixo;
				}

				free(pMatriz->pNoCorrente);

				/* Caso ja tenha varrido a matriz inteira, o ultimo no sera NULL */
				if (pProx != NULL)
				{
					pMatriz->pNoCorrente = pProx;
				}
			}
			direcao = 2;
			break;

		case 2:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pNoEsq != NULL)
				{
					pProx = pMatriz->pNoCorrente->pNoEsq;
				}
				else
				{
					pProx = pProx->pNoBaixo;
				}

				free(pMatriz->pNoCorrente);

				/* Caso ja tenha varrido a matriz inteira, o ultimo no sera NULL */
				if (pProx != NULL)
				{
					pMatriz->pNoCorrente = pProx;
				}
			}
			direcao = 1;
			break;
		}
	}

	free(pMatriz);
	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Funcão: MAT Obter Valor do No Corrente
*
***************************************************************************/

MAT_tpCondRet MAT_ObterValorCorr(MAT_tpMatriz *pMatriz, void **pValor)
{
	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	if (pMatriz->pNoCorrente->pValor == NULL)
	{
		pValor = NULL;
		return MAT_CondRetValNoNull;
	}

	*pValor = &pMatriz->pNoCorrente->pValor;
	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Funcão: MAT Inserir Valor no No
*
***************************************************************************/

MAT_tpCondRet MAT_InsereValor(MAT_tpMatriz *pMatriz, void *pValor)
{

	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	if (!pValor)
	{
		pMatriz->pNoCorrente->pValor = NULL;
	}
	else
	{
		pMatriz->pNoCorrente->pValor = pValor;
	}
	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Funcão: MAT Esvaziar Matriz
*
***************************************************************************/

MAT_tpCondRet MAT_EsvaziaMatriz(MAT_tpMatriz *pMatriz, int tam)
{
	/* Vari�vel auxiliar criada para indicar
	 * qual dire��o deve-se locomover para
	 * continuar deletando (free) os n�s.
	 */
	int direcao = 1;

	int j = -1;
	int i = -1;
	int nosNull = 0;

	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	pMatriz->pNoCorrente = pMatriz->pNoCabeca;

	for (i = 0; i < tam; i++)
	{
		switch (direcao)
		{
		case 1:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pValor != NULL)
				{
					pMatriz->pNoCorrente->pValor = NULL;
				}
				else
				{
					nosNull++;
				}

				if (pMatriz->pNoCorrente->pNoDir != NULL)
				{
					pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoDir;
				}
			}
			direcao = 2;
			break;

		case 2:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pValor != NULL)
				{
					pMatriz->pNoCorrente->pValor = NULL;
				}
				else
				{
					nosNull++;
				}

				if (pMatriz->pNoCorrente->pNoEsq != NULL)
				{
					pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoEsq;
				}
			}
			direcao = 1;
			break;
		}
		if (pMatriz->pNoCorrente->pNoBaixo != NULL)
		{
			pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixo;
		}
	}

	if (nosNull == tam * tam)
	{
		pMatriz->pNoCorrente = pMatriz->pNoCabeca;
		return MAT_CondRetMatrizVazia;
	}

	/* Ao finalizar, definimos o n� corrente como sendo o n� cabe�a. */
	pMatriz->pNoCorrente = pMatriz->pNoCabeca;
	return MAT_CondRetOK;
}

/* Codigo das funcões encapsuladas no modulo */

/***********************************************************************
*
*  $FC Fun��o: MAT Criar n� da Matriz
*
*  $FV Valor retornado
*     Ponteiro para o n� criado.
*     Ser� NULL caso a mem�ria tenha se esgotado.
*     Os ponteiros do n� criado estar�o nulos e o valor � igual ao do
*     par�metro.
*
***********************************************************************/

tpNoMatriz *CriaNo(int linha, int coluna)
{
	tpNoMatriz *pNoCorr;
	pNoCorr = (tpNoMatriz *)malloc(sizeof(tpNoMatriz));
	if (pNoCorr)
	{
		pNoCorr->coluna = coluna;
		pNoCorr->linha = linha;
		pNoCorr->pValor = NULL;
		pNoCorr->pNoBaixo = NULL;
		pNoCorr->pNoBaixoDir = NULL;
		pNoCorr->pNoBaixoEsq = NULL;
		pNoCorr->pNoCima = NULL;
		pNoCorr->pNoCimaDir = NULL;
		pNoCorr->pNoCimaEsq = NULL;
		pNoCorr->pNoDir = NULL;
		pNoCorr->pNoEsq = NULL;
#ifdef _DEBUG
		pNoCorr->pNoCab = NULL;
		pNoCorr->tipo = 1;
#endif

		return pNoCorr;
	}
	return NULL;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Liga n�s da Matriz
*  $FV Valor retornado
*     Inteiro indicando se a liga��o dos n�s
*	  ocorreu sem problemas.
*
***********************************************************************/

MAT_tpCondRet LigaNos(tpNoMatriz **pListaNo, int tam, tpNoMatriz *pCab)
{
	ePOS_posNo pos;
	int linha = -1;
	int coluna = -1;
	int tamAux = tam - 1;
	int tam2 = tam - tamAux;
	int i = 0;

	tpNoMatriz *pNoAdj = (tpNoMatriz *)malloc(sizeof(tpNoMatriz *));
	if (pNoAdj == NULL)
	{
		return MAT_CondRetFaltouMemoria;
	}

	/* Realiza a liga��o dos n�s baseado na posi��o dos mesmos. */
	for (i = 0; i < tam * tam; i++)
	{
		pos = EncontraPosicaoNo(*pListaNo[i], tam);

		switch (pos)
		{
		case (4):
			linha = tam;
			coluna = tam;

			/* Cima*/
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;
			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaEsq*/
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 4 */

		case (3):
			linha = tam;
			coluna = tam2;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 3 */

		/* Canto Sup Dir */
		case (2):
			linha = tam2;
			coluna = tam;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 2 */

		/* Canto Sup Esq */
		case (1):
			linha = tam2;
			coluna = tam2;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 1 */

		/* Extremimade Dir */
		case (8):
			linha = pListaNo[i]->linha;
			coluna = tam;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaEsq */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 8 */

		/* Extremidade Esq */
		case (7):
			linha = pListaNo[i]->linha;
			coluna = tam2;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 7 */

		/* Extremidade Inf */
		case (6):
			linha = tam;
			coluna = pListaNo[i]->coluna;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

			/* CimaEsq */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 6 */

		/* Extremidade Sup */
		case (5):
			linha = tam2;
			coluna = pListaNo[i]->coluna;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 5 */

		/* Generico */
		case (9):
			linha = pListaNo[i]->linha;
			coluna = pListaNo[i]->coluna;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaEsq */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;

#ifdef _DEBUG
			pListaNo[i]->pNoCab = pCab;
#endif
			break;
			/* Fim case 9 */
		}
	}
	return MAT_CondRetOK;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Encontrar Posi��o do N�
*
*  $FV Valor retornado
*     Inteiro referente � posi��o do n�.
*
***********************************************************************/

ePOS_posNo EncontraPosicaoNo(tpNoMatriz tpNo, int tam)
{
	int col;
	int lin;
	int tam2 = tam - 1;
	int tam3 = tam - tam2;
	col = tpNo.coluna;
	lin = tpNo.linha;

	if (lin == tam && col == tam3)
	{
		return POS_CantoInfEsq;
	}
	if (lin == tam3 && col == tam3)
	{
		return POS_CantoSupEsq;
	}
	if (lin == tam3 && col == tam)
	{
		return POS_CantoSupDir;
	}
	if (lin == tam && col == tam)
	{
		return POS_CantoInfDir;
	}
	if (lin == tam)
	{
		return POS_ExtremidadeInf;
	}
	if (lin == tam3)
	{
		return POS_ExtremidadeSup;
	}
	if (col == tam)
	{
		return POS_ExtremidadeDir;
	}
	if (col == tam3)
	{
		return POS_ExtremidadeEsq;
	}
	return POS_Generico;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Acha n� adjacente

*  $FV Valor retornado
*     Ponteiro para o n� adjacente presente na lista de n�s.
*
***********************************************************************/

tpNoMatriz *ProcuraNo(tpNoMatriz **pListaNo, int linha, int coluna, int tam)
{
	int i = -1;

	for (i = 0; i < tam * tam; i++)
	{
		if (pListaNo[i]->coluna == coluna && pListaNo[i]->linha == linha)
		{
			return pListaNo[i];
		}
	}

	return NULL;
}

#ifdef _DEBUG

/***********************************************************************
*
*  $FC Fun��o: MAT Verificar Estrutura
*
***********************************************************************/
int MAT_VerificarMatriz(MAT_tpMatriz *pMatriz)
{
	/* retorno da funcao, quantidade de erros  */
	int numErros = 0;

	/* 
	Vetor auxiliar para Verificacao das funcoes :
	0 - NoLixo
	1 - NoNUll
	2 - NullCabeca
	3 - Tipo
	4 - ConteudoNull
	*/
	int funcoes[5];

	/* Para o loop */
	int linha, coluna;

	/* auxiliar do loop */
	int tam;

	/* auxiliar para primeira coluna da matriz */
	tpNoMatriz *primeiraColuna = pMatriz->pNoCabeca;

	/* Auxiliar corredor */
	tpNoMatriz *corredor;

	/* define tam max do loop ( sqrt(tamNos) ) */
	tam = pMatriz->tamNos / pMatriz->tamNos;

	/* Inicializa "funcoes" */
	for (linha = 0; linha < 5; linha++)
	{
		funcoes[linha] = 0;
	}

	/* Erro 1 */
	numErros += Ver_CorrenteNull(pMatriz->pNoCorrente);

	/* Erro 2 */
	numErros += Ver_LibertoSemFree(pMatriz->pNoCorrente);

	for (coluna = 0; coluna < tam; coluna++)
	{
		/* aciona o corredor como o primeiro no da coluna extrema esquerda */
		corredor = primeiraColuna;

		for (linha = 0; linha < tam; linha++)
		{
			/* primeira coluna */
			if (!coluna)
			{
#ifdef _DEBUG
				CNT_Contar("loop - primeira Coluna", __LINE__);
#endif
				/* primeira linha */
				if (!linha)
				{
#ifdef _DEBUG
					CNT_Contar("loop - primeira colunaprimeira - Linha", -__LINE__);
#endif
					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoInfEsq) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeEsq) &&
						   Ver_NoLixo(corredor, POS_CantoSupEsq) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeSup) &&
						   Ver_NoLixo(corredor, POS_CantoSupDir)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeDir) ||
						  Ver_NoLixo(corredor, POS_CantoInfDir) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeInf))))
					{
#ifdef _DEBUG
						CNT_Contar("loop - primeira colunaprimeira - Linha Lixo- ", __LINE__);
#endif

						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop - primeira colunaprimeira - Linha else - Lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoInfEsq) &&
						   Ver_NoNull(corredor, POS_ExtremidadeEsq) &&
						   Ver_NoNull(corredor, POS_CantoSupEsq) &&
						   Ver_NoNull(corredor, POS_ExtremidadeSup) &&
						   Ver_NoNull(corredor, POS_CantoSupDir)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeDir) ||
						  Ver_NoNull(corredor, POS_CantoInfDir) ||
						  Ver_NoNull(corredor, POS_ExtremidadeInf))))
					{
#ifdef _DEBUG
						CNT_Contar("loop - primeira coluna - primeira Linha - else Null", __LINE__);
#endif

						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop - primeira coluna - primeira Linha - else Null2", __LINE__);
					}
#endif
				}
				else if ((linha + 1) == tam) /* ultima linha */
				{
#ifdef _DEBUG
					CNT_Contar("loop - primeira coluna - ultima Linha- ", __LINE__);
#endif

					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoInfDir) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeInf) &&
						   Ver_NoLixo(corredor, POS_CantoInfEsq) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeEsq) &&
						   Ver_NoLixo(corredor, POS_CantoSupEsq)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeSup) ||
						  Ver_NoLixo(corredor, POS_CantoSupDir) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeDir))))
					{
#ifdef _DEBUG
						CNT_Contar("loop - primeira coluna - ultima Linha - Lixo", __LINE__);
#endif

						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop - primeira coluna - ultima Linha - else Lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoInfDir) &&
						   Ver_NoNull(corredor, POS_ExtremidadeInf) &&
						   Ver_NoNull(corredor, POS_CantoInfEsq) &&
						   Ver_NoNull(corredor, POS_ExtremidadeEsq) &&
						   Ver_NoNull(corredor, POS_CantoSupEsq)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeSup) ||
						  Ver_NoNull(corredor, POS_CantoSupDir) ||
						  Ver_NoNull(corredor, POS_ExtremidadeDir))))
					{
#ifdef _DEBUG
						CNT_Contar("loop - primeira coluna - ultima Linha - Null", __LINE__);
#endif

						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop - primeira coluna - ultima Linha - else Null", __LINE__);
					}
#endif
				}
				else /* linhas do meio */
				{
#ifdef _DEBUG
					CNT_Contar("loop - primeira coluna - meio Linhas- ", __LINE__);
#endif

					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoInfEsq) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeEsq) &&
						   Ver_NoLixo(corredor, POS_CantoSupEsq)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeSup) ||
						  Ver_NoLixo(corredor, POS_CantoSupDir) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeDir) ||
						  Ver_NoLixo(corredor, POS_CantoInfDir) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeInf))))
					{
#ifdef _DEBUG
						CNT_Contar("loop - primeira coluna - meio Linhas - Lixo", __LINE__);
#endif

						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop - primeira coluna - meio Linhas - else Lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoInfEsq) &&
						   Ver_NoNull(corredor, POS_ExtremidadeEsq) &&
						   Ver_NoNull(corredor, POS_CantoSupEsq)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeSup) ||
						  Ver_NoNull(corredor, POS_CantoSupDir) ||
						  Ver_NoNull(corredor, POS_ExtremidadeDir) ||
						  Ver_NoNull(corredor, POS_CantoInfDir) ||
						  Ver_NoNull(corredor, POS_ExtremidadeInf))))
					{
#ifdef _DEBUG
						CNT_Contar("loop - primeira coluna - meio Linhas - Null", __LINE__);
#endif

						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop - primeira coluna - meio Linhas - else Lixo2", __LINE__);
					}

#endif
				}
			}
			else if ((coluna + 1) == tam) /* ultima coluna */
			{
#ifdef _DEBUG
				CNT_Contar("loop -  ultima coluna", __LINE__);
#endif

				/* primeira linha */
				if (!linha)
				{
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoSupEsq) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeSup) &&
						   Ver_NoLixo(corredor, POS_CantoSupDir) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeDir) &&
						   Ver_NoLixo(corredor, POS_CantoInfDir)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeInf) ||
						  Ver_NoLixo(corredor, POS_CantoInfEsq) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeEsq))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  ultima coluna - primeira linha - Lixo", __LINE__);
#endif
						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  ultima coluna - primeira linha - else Lixo", __LINE__);
					}
#endif

					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoSupEsq) &&
						   Ver_NoNull(corredor, POS_ExtremidadeSup) &&
						   Ver_NoNull(corredor, POS_CantoSupDir) &&
						   Ver_NoNull(corredor, POS_ExtremidadeDir) &&
						   Ver_NoNull(corredor, POS_CantoInfDir)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeInf) ||
						  Ver_NoNull(corredor, POS_CantoInfEsq) ||
						  Ver_NoNull(corredor, POS_ExtremidadeEsq))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  ultima coluna - primeira linha - null", __LINE__);
#endif

						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  ultima coluna - primeira linha - else null", __LINE__);
					}
#endif
				}
				else if ((linha + 1) == tam) /* ultima linha */
				{
					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoSupDir) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeDir) &&
						   Ver_NoLixo(corredor, POS_CantoInfDir) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeInf) &&
						   Ver_NoLixo(corredor, POS_CantoInfEsq)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeEsq) ||
						  Ver_NoLixo(corredor, POS_CantoSupEsq) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeSup))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  ultima coluna - ultima linha - lixo", __LINE__);
#endif
						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  ultima coluna - ultima linha - else lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoSupDir) &&
						   Ver_NoNull(corredor, POS_ExtremidadeDir) &&
						   Ver_NoNull(corredor, POS_CantoInfDir) &&
						   Ver_NoNull(corredor, POS_ExtremidadeInf) &&
						   Ver_NoNull(corredor, POS_CantoInfEsq)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeEsq) ||
						  Ver_NoNull(corredor, POS_CantoSupEsq) ||
						  Ver_NoNull(corredor, POS_ExtremidadeSup))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  ultima coluna - ultima linha - null", __LINE__);
#endif
						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  ultima coluna - ultima linha - else null", __LINE__);
					}
#endif
				}
				else /* linhas do meio */
				{
					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoSupDir) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeDir) &&
						   Ver_NoLixo(corredor, POS_CantoInfDir)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeInf) ||
						  Ver_NoLixo(corredor, POS_CantoInfEsq) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeEsq) ||
						  Ver_NoLixo(corredor, POS_CantoSupEsq) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeSup))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  ultima coluna - meio linha - lixo", __LINE__);
#endif
						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  ultima coluna - meio linha - else lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoSupDir) &&
						   Ver_NoNull(corredor, POS_ExtremidadeDir) &&
						   Ver_NoNull(corredor, POS_CantoInfDir)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeInf) ||
						  Ver_NoNull(corredor, POS_CantoInfEsq) ||
						  Ver_NoNull(corredor, POS_ExtremidadeEsq) ||
						  Ver_NoNull(corredor, POS_CantoSupEsq) ||
						  Ver_NoNull(corredor, POS_ExtremidadeSup))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  ultima coluna - meio linha - null", __LINE__);
#endif
						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  ultima coluna - meio linha - else null", __LINE__);
					}
#endif
				}
			}
			else /* colunas do meio */
			{
#ifdef _DEBUG
				CNT_Contar("loop -  meio coluna", __LINE__);
#endif
				/* primeira linha */
				if (!linha)
				{
					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoSupEsq) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeSup) &&
						   Ver_NoLixo(corredor, POS_CantoSupDir)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeDir) ||
						  Ver_NoLixo(corredor, POS_CantoInfDir) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeInf) ||
						  Ver_NoLixo(corredor, POS_CantoInfEsq) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeEsq))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  meio coluna - primeira linha - lixo", __LINE__);
#endif
						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  meio coluna - primeira linha - else lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoSupEsq) &&
						   Ver_NoNull(corredor, POS_ExtremidadeSup) &&
						   Ver_NoNull(corredor, POS_CantoSupDir)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeDir) ||
						  Ver_NoNull(corredor, POS_CantoInfDir) ||
						  Ver_NoNull(corredor, POS_ExtremidadeInf) ||
						  Ver_NoNull(corredor, POS_CantoInfEsq) ||
						  Ver_NoNull(corredor, POS_ExtremidadeEsq))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  meio coluna - primeira linha - null", __LINE__);
#endif
						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  meio coluna - primeira linha - else null", __LINE__);
					}
#endif
				}
				else if ((linha + 1) == tam) /* ultima linha */
				{
					/* Erro 3 */
					if (
						!funcoes[0] &&
						(!(Ver_NoLixo(corredor, POS_CantoInfDir) &&
						   Ver_NoLixo(corredor, POS_ExtremidadeInf) &&
						   Ver_NoLixo(corredor, POS_CantoInfEsq)) ||
						 (Ver_NoLixo(corredor, POS_ExtremidadeDir) ||
						  Ver_NoLixo(corredor, POS_CantoSupEsq) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeSup) ||
						  Ver_NoLixo(corredor, POS_CantoSupDir) ||
						  Ver_NoLixo(corredor, POS_ExtremidadeEsq))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  meio coluna - ultima linha - lixo", __LINE__);
#endif
						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  meio coluna - ultima linha - else lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(!(Ver_NoNull(corredor, POS_CantoInfDir) &&
						   Ver_NoNull(corredor, POS_ExtremidadeInf) &&
						   Ver_NoNull(corredor, POS_CantoInfEsq)) ||
						 (Ver_NoNull(corredor, POS_ExtremidadeDir) ||
						  Ver_NoNull(corredor, POS_CantoSupEsq) ||
						  Ver_NoNull(corredor, POS_ExtremidadeSup) ||
						  Ver_NoNull(corredor, POS_CantoSupDir) ||
						  Ver_NoNull(corredor, POS_ExtremidadeEsq))))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  meio coluna - ultima linha - null", __LINE__);
#endif
						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  meio coluna - ultima linha - else null", __LINE__);
					}
#endif
				}
				else /* linhas do meio */
				{
					/* Erro 3 */
					if (
						!funcoes[0] &&
						(Ver_NoLixo(corredor, POS_CantoInfDir) ||
						 Ver_NoLixo(corredor, POS_ExtremidadeInf) ||
						 Ver_NoLixo(corredor, POS_CantoInfEsq) ||
						 Ver_NoLixo(corredor, POS_ExtremidadeDir) ||
						 Ver_NoLixo(corredor, POS_CantoSupEsq) ||
						 Ver_NoLixo(corredor, POS_ExtremidadeSup) ||
						 Ver_NoLixo(corredor, POS_CantoSupDir) ||
						 Ver_NoLixo(corredor, POS_ExtremidadeEsq)))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  meio coluna - meio linha - lixo", __LINE__);
#endif
						numErros += 1;
						funcoes[0] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  meio coluna - meio linha - else lixo", __LINE__);
					}
#endif

					/* Erro 4 */
					if (
						!funcoes[1] &&
						(Ver_NoNull(corredor, POS_CantoInfDir) ||
						 Ver_NoNull(corredor, POS_ExtremidadeInf) ||
						 Ver_NoNull(corredor, POS_CantoInfEsq) ||
						 Ver_NoNull(corredor, POS_ExtremidadeDir) ||
						 Ver_NoNull(corredor, POS_CantoSupEsq) ||
						 Ver_NoNull(corredor, POS_ExtremidadeSup) ||
						 Ver_NoNull(corredor, POS_CantoSupDir) ||
						 Ver_NoNull(corredor, POS_ExtremidadeEsq)))
					{
#ifdef _DEBUG
						CNT_Contar("loop -  meio coluna - meio linha - null", __LINE__);
#endif
						numErros += 1;
						funcoes[1] = 1;
					}
#ifdef _DEBUG
					else
					{
						CNT_Contar("loop -  meio coluna - meio linha - else null", __LINE__);
					}
#endif
				}
			}

			/* Erro 5 */
			if (!funcoes[2] && Ver_NullCabeca(corredor))
			{

				numErros += 1;
				funcoes[2] = 1;
			}
#ifdef _DEBUG
			else
			{
				CNT_Contar("loop - geral - else Cabeca null", __LINE__);
			}
#endif

			/* Erro 6 */
			if (!funcoes[3] && Ver_Tipo(corredor->tipo))
			{
#ifdef _DEBUG
				CNT_Contar("loop - geral - tipo", __LINE__);
#endif
				numErros += 1;
				funcoes[3] = 1;
			}
#ifdef _DEBUG
			else
			{
				CNT_Contar("loop - geral - else tipo", __LINE__);
			}
#endif

			/* Erro 7 */
			if (!funcoes[4] && Ver_ConteudoNull(corredor->pValor))
			{
#ifdef _DEBUG
				CNT_Contar("loop - geral - Conteudo null", __LINE__);
#endif
				numErros += 1;
				funcoes[4] = 1;
			}
#ifdef _DEBUG
			else
			{
				CNT_Contar("loop - geral - else Conteudo null", __LINE__);
			}
#endif

			corredor = corredor->pNoDir;
		}

		/* desce o indicador da coluna da extrema esquerda */
		primeiraColuna = primeiraColuna->pNoBaixo;
	}

	return numErros;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Deturpar Estrutura
*
***********************************************************************/
MAT_tpCondRet MAT_Deturpar(MAT_tpMatriz *pMatriz, int tipoDeturpacao, int direcao, int lixo)
{
	switch (tipoDeturpacao)
	{
	/* Atribuir NULL ou lixo a algum no. */
	case 1:
		switch (direcao)
		{
			/* CimaEsq */
		case 1:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoCimaEsq = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoCimaEsq = NULL;
				break;
			}

			/* CimaDir */
		case 2:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoCimaDir = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoCimaDir = NULL;
				break;
			}

			/* BaixoEsq */
		case 3:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoBaixoEsq = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoBaixoEsq = NULL;
				break;
			}

			/* BaixoDir */
		case 4:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoBaixoDir = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoBaixoDir = NULL;
				break;
			}

			/* Cima */
		case 5:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoCima = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoCima = NULL;
				break;
			}

			/* Baixo */
		case 6:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoBaixo = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoBaixo = NULL;
				break;
			}

			/* Esquerda */
		case 7:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoEsq = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoEsq = NULL;
				break;
			}

			/* Direita */
		case 8:
			if (lixo == 1)
			{
				pMatriz->pNoCorrente->pNoDir = -1;
				break;
			}
			else
			{
				pMatriz->pNoCorrente->pNoDir = NULL;
				break;
			}
			/* Nulo: No Corrente*/
		case 9:
			pMatriz->pNoCorrente = NULL;
			break;
		}
		break;

	/* Free no elemento corrente. */
	case 2:
		free(pMatriz->pNoCorrente);
		break;

	/* Altera o tipo no valor contido no no. */
	case 3:
		pMatriz->pNoCorrente->tipo = -1;
		break;

	/* Desencadeia o no sem dar free. */
	case 4:
		DesencadearNo(pMatriz->pNoCorrente);
		break;

	/* Free no no de origem (cabeca) */
	case 5:
		pMatriz->pNoCabeca = NULL;
		break;
	}

	return MAT_CondRetOK;
}

/* **********

$FC Fun��o: todos nos adjacentes apontarem para null,
e faz o no corrente apontar para NULL em todas as direcoes.

*********** */
void DesencadearNo(tpNoMatriz *tpNo)
{
	tpNo->pNoDir = NULL;
	tpNo->pNoCimaDir = NULL;
	tpNo->pNoCima = NULL;
	tpNo->pNoCimaEsq = NULL;
	tpNo->pNoEsq = NULL;
	tpNo->pNoBaixoEsq = NULL;
	tpNo->pNoBaixo = NULL;
	tpNo->pNoBaixoDir = NULL;
}

static int Ver_CorrenteNull(tpNoMatriz *pNoMatriz)
{
	if (!pNoMatriz)
	{
		return 1;
	}
	return 0;
}

static int Ver_NoNull(tpNoMatriz *pNoMatriz, ePOS_posNo direcao)
{
	switch (direcao)
	{
	case POS_CantoInfEsq:
		if (!pNoMatriz->pNoBaixoEsq)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeEsq:
		if (!pNoMatriz->pNoEsq)
		{
			return 1;
		}
		return 0;

	case POS_CantoSupEsq:
		if (!pNoMatriz->pNoCimaEsq)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeSup:
		if (!pNoMatriz->pNoCima)
		{
			return 1;
		}
		return 0;

	case POS_CantoSupDir:
		if (!pNoMatriz->pNoCimaDir)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeDir:
		if (!pNoMatriz->pNoDir)
		{
			return 1;
		}
		return 0;

	case POS_CantoInfDir:
		if (!pNoMatriz->pNoBaixoDir)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeInf:
		if (!pNoMatriz->pNoBaixo)
		{
			return 1;
		}
		return 0;
	}
}

static int Ver_NoLixo(tpNoMatriz *pNoMatriz, ePOS_posNo direcao)
{
	switch (direcao)
	{
	case POS_CantoInfEsq:
		if (!pNoMatriz->pNoBaixoEsq || pNoMatriz->pNoBaixoEsq->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeEsq:
		if (!pNoMatriz->pNoEsq || pNoMatriz->pNoEsq->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_CantoSupEsq:
		if (!pNoMatriz->pNoCimaEsq || pNoMatriz->pNoCimaEsq->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeSup:
		if (!pNoMatriz->pNoCima || pNoMatriz->pNoCima->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_CantoSupDir:
		if (!pNoMatriz->pNoCimaDir || pNoMatriz->pNoCimaDir->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeDir:
		if (!pNoMatriz->pNoDir || pNoMatriz->pNoDir->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_CantoInfDir:
		if (!pNoMatriz->pNoBaixoDir || pNoMatriz->pNoBaixoDir->tipo < 0)
		{
			return 1;
		}
		return 0;

	case POS_ExtremidadeInf:
		if (!pNoMatriz->pNoBaixo || pNoMatriz->pNoBaixo->tipo < 0)
		{
			return 1;
		}
		return 0;
	}
}

static int Ver_ConteudoNull(void *pValor)
{
	char val = (char)pValor;

	if (val)
	{
		return 0;
	}
	return 1;
}

static int Ver_Tipo(int tipo)
{
	if (tipo < 0)
	{
		return 1;
	}
	return 0;
}

static int Ver_LibertoSemFree(tpNoMatriz *pNoMatriz)
{
	if (
		pNoMatriz->pNoBaixo != NULL ||
		pNoMatriz->pNoBaixoDir != NULL ||
		pNoMatriz->pNoBaixoEsq != NULL ||
		pNoMatriz->pNoCima != NULL ||
		pNoMatriz->pNoCimaDir != NULL ||
		pNoMatriz->pNoCimaEsq != NULL ||
		pNoMatriz->pNoEsq != NULL ||
		pNoMatriz->pNoDir != NULL)
	{
		return 1;
	}
	return 0;
}

static int Ver_NullCabeca(MAT_tpMatriz *pNoMatriz)
{
	if (pNoMatriz->pNoCorrente->pNoCab != pNoMatriz->pNoCabeca)
	{
		return 1;
	}
	return 0;
}
#endif
