/**************************************************************
*
*   M�dulo de Definicao : Modulo Principal

*   Autores:    glg - Guilherme de Lacerda Gomes
*		        mrp - Matheus Romero Paixão
*               fac - Fernanda de Almeida Castro
*
*   Hist�rico de evolu��o:
*	Legenda :
*		HotFixes, Atualizações e enums 		: Versão += 0.0.1 
*		Adições e/ou remoções de funções 	: Versão += 0.1.0
*		Finalização do Trabalho 			: Versão += 1.0.0
*
*   Vers�o      Autor       Data        Observa��es
*   1.0.0        mrp		29/10/2019  Inicio de desenvolvimento. (30 mim)
*   1.1.0        mrp        30/10/2019  Organizacao do modulo (+- 4 hrs)
*   1.1.3        mrp        30/10/2019  Organizacao do modulo (+- 4 hrs)
*	1.1.4		 glg		03/11/2019	Realizacao de testes com a funcao de desvendar o labirinto.
*   1.1.5        mrp        06/11/2019  Adicao de retornos alternativos no codigo e Revisao Geral (4 hrs)
*   
*
*
*   Descri��o do M�dulo:
*		Esse modulo visa implementar a interacao com o usuario oferecendo  
*		a criacao e modificacao do labirinto e uma solucao para o labirinto criado  
*
***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#include "Labirinto\labirinto.h"

/* Definicoes do modulo */

/* Teclas */
#define SETA_ESQUERDA 75
#define SETA_DIREITA 77
#define SETA_CIMA 72
#define SETA_BAIXO 80

#define ENTRADA 101
#define SAIDA 115
#define CAMINHO 99
#define PAREDE 112

#define CONTINUAR 32
#define VOLTAR 48

/* Checkpoint Labirinto */
#define LAB_PAREDE 0
#define LAB_ENTRA 1
#define LAB_SAIDA 2
#define LAB_CAMINHO 3
#define GOD 0

/* Direcoes */
#define CIMA 5
#define BAIXO 6
#define ESQUERDA 7
#define DIREITA 8

/* Retorno do Labirinto */
#define LAB_CondRetOK 0
#define LAB_CondRetMovimentacaoInvalida 1
#define LAB_CondRetLabirintoNaoExiste 2
#define LAB_CondRetFaltouMemoria 3
#define LAB_CondRetFimLabirinto 4
#define LAB_CondRetEhParede 5
#define LAB_CondRetTemCheckpoint 6
#define LAB_CondRetErroNaAcao 7
#define LAB_CondRetAcaoInvalida 8
#define LAB_CondRetTamanhoInvalido 9

/* Prototipo das Funcoes encapsuladas no modulo Principal */

/* Funcao que faz o terminal ficar com a tela limpa depois determinado tempo */
void LimpaTela(int tempo);

/* Enum para identificar as "voltas" que o usuario quiser realizar no programa */
typedef enum eRec_recomeco
{
	eRec_Introducao = 105,

	eRec_ColetaDados = 100

} eRec_Recomeco;

/* **************************************** */

int main()
{
	/* Variantes Auxiliares */
	int tecla, time;

	/* Verifica retornos */
	LAB_tpCondRet retorno;

	/* Identifica localizacao do usuario no labirinto */
	int usuColuna, usuLinha;

	/* Usuario define linha e coluna do labirinto  */
	int linha, coluna;

	/* Labirinto a ser utilizado pelo usuario */
	LAB_tppLabirinto LabUser;

	LabUser = (LAB_tppLabirinto)malloc(sizeof(LAB_tppLabirinto));
	if (!LabUser)
	{
		puts("Labirinto nao foi criado por falta de memoria, pressione qualquer tecla para fechar o programa");
		_getch();
		exit(0);
	}

	tecla = eRec_Introducao;
	/* laco de repeticao indefinida para que o usuario posso recomecar e testar outros labirintos
    antes que o programa seja fechado */
	while (1)
	{
		/* Introducao */
		if (tecla == eRec_Introducao)
		{

			LimpaTela(0);

			puts("BEM VINDO AO TRABALHO 3 (LABIRINTO PARTE II) !!!");
			puts("Grupo : ");
			puts("\tFernanda de Almeida Castro; ");
			puts("\tGuilherme de Lacerda Gomes; ");
			puts("\tMatheus Romero Paixao ");
			puts("Espaco para continuar ");
			_getch();
			puts("Carregando...");

			tecla = eRec_ColetaDados;
		}

		/* Coleta de dados e verificacao de dados (redimensionar) */
		if (tecla == eRec_ColetaDados)
		{
			usuColuna = usuLinha = 0;
			/* Alocacao para o labirinto ( AlocaLab() ) */
			LabUser = (LAB_tppLabirinto)malloc(sizeof(LAB_tppLabirinto));
			if (!LabUser)
			{
				puts("Labirinto nao foi criado por falta de memoria"
					 ", pressione qualquer tecla para fechar o programa");
				_getch();
				exit(0);
			}

			/* enquanto for pressionado 0 ou tamanho for invalido sera pedido o coluna e linha do labirinto  */
			do
			{
				LimpaTela(1);
				puts("Informe a quantidade (1~10) de linhas para o seu labirinto (Enter para avancar): ");
				scanf_s("%d", &linha);
				puts("Salvando...");

				LimpaTela(1);
				puts("Informe a quantidade (1~10) de colunas para o seu labirinto (Enter para avancar): ");
				scanf_s("%d", &coluna);
				puts("Salvando...");

				LimpaTela(1);
				retorno = LAB_CriarLabirinto(LabUser, linha, coluna);

				switch (retorno)
				{
				case LAB_CondRetTamanhoInvalido:
					puts("Tamanho invalido, por favor insira valores validos .");
					LimpaTela(2);
					tecla = VOLTAR;
					puts("Voltando...");
					break;

				case LAB_CondRetOK:
					printf(
						"Linhas %d \n"
						"Colunas %d \n"
						"Deseja alterar ? (Pressione 0 para Alterar ou qualquer tecla para Continuar )\n",
						linha, coluna);

					tecla = _getch();
					puts("Carregando...");

					break;

				case LAB_CondRetFaltouMemoria:
					puts("Faltou memoria ! Pressione qualquer tecla pra fechar o programa .");
					_getch();
					exit(0);

				default:
					puts("Ocorreu algum erro inesperado ! Pressione qualquer tecla pra fechar o programa .");
					_getch();
					exit(0);
				}

			} while (tecla == VOLTAR);

			LimpaTela(1);
			puts("Coluna e Linha confirmados !");
			puts("Labirinto criado com sucesso !");
			LimpaTela(2);
		}

		do
		{
			/* valor padrao para identificar seta */
			if (tecla != 224)
			{
				LimpaTela(0);
				LAB_MostraLabirinto(LabUser, usuLinha, usuColuna);

				puts("_________________________________________________________________________\n "
					 "Identificadores :\t\t\tUse as SETAS para andar pelo labirinto ou :\n "
					 "\t\tO = sua posicao\n "
					 "X = parede \t\t   E = entrada \t\tc = Caminho \t\t  e = Entrada \n "
					 "  = caminho\t\t   S = saida   \t\tp = Parede  \t\t  s = Saida   \n "
					 "Pressione Espaco para concluir e Passar para o Desvendador. \n");
			}

			tecla = _getch();
			if (tecla != 224)
			{
				puts("Carregando...");

				switch (tecla)
				{
				case 224:
					retorno = (LAB_tpCondRet)LAB_CondRetOK;
					break;

				case CONTINUAR:
					puts("Labirinto concluido pelo usuario !");
					retorno = (LAB_tpCondRet)LAB_CondRetOK;
					break;

				case ENTRADA:
					retorno = LAB_ModificarGrid(LabUser, LAB_ENTRA);

					if (retorno == LAB_CondRetTemCheckpoint)
					{
						puts("Labirinto ja contem entrada, transforme primeiro a entrada para parede/caminho e defina novamente depois.");
						LimpaTela(2);
						break;
					}
					break;

				case SAIDA:
					retorno = LAB_ModificarGrid(LabUser, LAB_SAIDA);

					if (retorno == LAB_CondRetTemCheckpoint)
					{
						puts("Labirinto ja contem saida, transforme primeiro a saida para parede/caminho e defina novamente depois.");
						LimpaTela(2);
						break;
					}
					break;

				case CAMINHO:
					retorno = LAB_ModificarGrid(LabUser, LAB_CAMINHO);
					break;

				case PAREDE:
					retorno = LAB_ModificarGrid(LabUser, LAB_PAREDE);
					break;

				case SETA_DIREITA:
					retorno = LAB_AndarPeloLab(LabUser, DIREITA, GOD);
					if (retorno != LAB_CondRetAcaoInvalida)
					{
						usuColuna += 1;
					}
					break;

				case SETA_ESQUERDA:
					retorno = LAB_AndarPeloLab(LabUser, ESQUERDA, GOD);
					if (retorno != LAB_CondRetAcaoInvalida)
					{
						usuColuna -= 1;
					}
					break;

				case SETA_CIMA:
					retorno = LAB_AndarPeloLab(LabUser, CIMA, GOD);
					if (retorno != LAB_CondRetAcaoInvalida)
					{
						usuLinha -= 1;
					}
					break;

				case SETA_BAIXO:
					retorno = LAB_AndarPeloLab(LabUser, BAIXO, GOD);
					if (retorno != LAB_CondRetAcaoInvalida)
					{
						usuLinha += 1;
					}
					break;

				default:
					puts("Insira um comando valido! ");
					LimpaTela(1);
					retorno = (LAB_tpCondRet)LAB_CondRetOK;
					break;
				}
				if (retorno != LAB_CondRetOK)
				{
					puts("Acao nao pode ser concluida por algum erro, verifique o labirinto e insira um novo comando.\nVoltando...");
					tecla = VOLTAR;
					LimpaTela(2);
				}

				if ((tecla == CONTINUAR) && (LAB_VerificaCheckpoints(LabUser, 1) != LAB_CondRetOK))
				{
					puts("A insercao de todos os checkpoints (Entrada e Saida) e obrigatorio!\nVoltando...");
					LimpaTela(2);
					tecla = VOLTAR;
				}
			}

			if ((tecla == CONTINUAR) && (LAB_VerificaCheckpoints(LabUser, 0) != LAB_CondRetOK))
			{
				puts("Por favor, retorne com a sua posicao para a entrada do labirinto ! ;) ");
				for (time = 0; time < 3; time++)
				{
					printf(" .");
					Sleep(800);
				}
				tecla = VOLTAR;
			}
		} while (tecla != CONTINUAR);

		puts("Carregando ... ");
		LimpaTela(1);
		LAB_MostraLabirinto(LabUser, usuLinha, usuColuna);
		puts("Esse e o seu labirinto finalizado!");
		printf("Desvendando Labirinto");

		for (time = 0; time < 3; time++)
		{
			Sleep(800);
			printf(" .");
		}

		LimpaTela(1);
		LAB_MostraLabirinto(LabUser, usuLinha, usuColuna);
		puts("_________________________________________________\n");
		LAB_DesvendaLabirinto(LabUser);

		while (1)
		{
			puts("\nSeu labirinto foi averiguado e resolvido;");
			puts("Escolha uma Opcao (Pressione):");
			puts("Introducao  =  i ;");
			puts("Redimensionamento do Labirinto   =  d ; (linhas e colunas) ");
			puts("Para Sair  =  0 ;");
			tecla = _getch();
			LimpaTela(0);
			if (tecla == eRec_ColetaDados || tecla == eRec_Introducao || tecla == VOLTAR)
			{
				break;
			}
		}

		/* Exclui labirinto atual para a criação de um novo Labirinto definido pelo usuario */
		LAB_ExcluiLabirinto(LabUser);

		if (tecla == VOLTAR)
		{
			break;
		}
	}

	LimpaTela(0);
	puts("Saindo .");
	LimpaTela(2);

	return 0;
}

/* Funcoes encapsuladas no modulo */

void LimpaTela(int tempo)
{
	Sleep(tempo * 1000);
	system("cls");
}
