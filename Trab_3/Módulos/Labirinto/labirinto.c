﻿/***********************************************************************
*  $MCI Módulo de implementação: Módulo labirinto
*
*  Arquivo gerado:              LABIRINTO.C
*  Letras identificadoras:      LAB
*
*  Projeto: Disciplina INF1301
*  Gestor:  DI/PUC-Rio
*  Autores:
*		glg - Gulherme de Lacerda Gomes
*       mrp - Matheus Romero Paixão
*       fac - Fernanda de Almeida Castro
*
*  $HA Historico de evolucao:
*     Versao  Autor   	 Data     	Observacoes
*       1.0.0    glg   	18/09/2019 	Inicio do desenvolvimento
*		1.1.0	 glg   	22/09/2019 	Adicao de mais funcoes ao modulo.
*		2.0.0	 glg   	26/09/2019 	Refatoracao completa do modulo labrinto.
*		2.0.1    mrp   	27/09/2019	Reorganização do Módulo
*		2.1.0	 mrp	29/09/2019	Adição e refatorações de funções
*		2.2.0    glg    30/09/2019  Implementação das últimas funcionalidades e revisão do módulo.
*		2.2.1    mrp	01/10/2019  Hotfixes
*		2.2.2    glg    01/10/2019  Revisão de código
*		2.3.0    glg    02/10/2019  Correção da função VerificaCelula e adição de verificação dos limites do labirinto.
*       2.3.1    fac    03/10/2019  Revisão de código
*		2.3.2	 mrp	04/10/2019	Revisão de código
*		2.3.3	 mrp	06/10/2019	Revisão de código
*		2.3.4	 glg	06/10/2019	Revisão de código
*		2.4.0	 mrp	30/10/2019 	Adicao de pilha e type pilha para desvendador
*		2.4.3	 mrp	31/10/2019 	Hotfixes, adicao de atributos globais
*		2.4.4	 glg	01/11/2019  Inicio da Implementacao do desvendador e revisao de codigo.
*		2.5.0	 glg	02/11/2019  Termino da Implementacao do desvendador, mudanca no tipo pilha e correcoes de formatacao
*		2.6.0	 glg	05/11/2019	Correcao de bugs do desvendador. Implementacao de novas funcoes e revisao de codigo.
*		2.6.4	 mrp 	06/11/2019	Revisao de codigo
*		2.6.5	 glg	06/11/2019  Implementacao de funcoes extras
***************************************************************************/

#include "..\Matriz\matriz.h"

#define LABIRINTO_OWN
#include "labirinto.h"
#undef LABIRINTO_OWN

#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor da direção de movimento
*
*
*  $ED Descrição do tipo
*    Cada inteiro representa uma direção desejada de locomoção,
*	o tipo corrente foi criado a fim de auxiliar na função verifica
*	célula, que no caso, se refere a própria célula
*
***********************************************************************/
typedef enum
{
	eLAB_Corrente = -1,
	eLAB_cima = 5,
	eLAB_baixo = 6,
	eLAB_direita = 8,
	eLAB_esquerda = 7

} eLAB_direcao;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor do tipo de célula.
*
*
*  $ED Descrição do tipo
*    Cada inteiro representa um tipo de célula. O tipo inválido foi criado a fim
*	 de auxiliar no retorno da função VerificaCelula, para o caso da célula não
*	 fazer parte do grid.
*
***********************************************************************/
typedef enum
{
	eLAB_Parede = 0,
	eLAB_Entrada = 1,
	eLAB_Saida = 2,
	eLAB_Caminho = 3,
	eLAB_Invalido = 4

} eLAB_tpCelula;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor do tipo de movimentação
*
*
*  $ED Descrição do tipo
*    Cada inteiro representa um tipo de movimentação. O tipo livre permite a locomoção em
*	 qualquer direção (não somente Cima, Baixo, Esquerda, Direita), e também permite "andar"
*	 por paredes. O mesmo foi feito para facilitar a criação do labirinto.
*	 Já o tipo restrito é o tipo de movimento do usuário durante a movimentação pelo
*	 labirinto já construído.
*
***********************************************************************/
typedef enum
{
	eLAB_MovLivre = 0,
	eLAB_MovRestrito = 1

} eLAB_tpMovimentacao;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor da cabeca de um labirinto.
*
*
*  $ED Descricao do tipo
*     O tipo refere-se a cabeca de um labirinto, contendo um ponteiro para sua
*	  matriz base (matriz que representa as paredes, caminhos, entrada e saida),
*	  as coordenadas de entrada e saida e o tamanho do labirinto.
*
***********************************************************************/
typedef struct LAB_tagLabirinto
{
	/*  Ponteiro para uma matriz que contem
	 *  armazenado os caminhos, paredes,
	 *  entrada e saida do labirinto.
	 */
	MAT_tppMatriz pMatrizBase;

	/* Inteiro referente ao número de linhas */
	int LAB_linhas;

	/* Inteiro referente ao número de colunas */
	int LAB_colunas;

} LAB_tpLabirinto;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor de uma célula do labirinto
*
*
*  $ED Descrição do tipo
*    Inteiro que representa o tipo da célula (Parede, Entrada, Saída, Caminho).
*
***********************************************************************/
typedef struct LAB_tagCelulaLabirinto
{
	/* Contém o tipo de célula do grid (parede, entrada, saída ou caminho) */
	eLAB_tpCelula tpCelula;

	/* Variavel auxiliar para o desvendador  */
	int visitado;

} LAB_tpCelulaLabirinto;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor de uma pilha contendo a solucao
*
*
*  $ED Descrição do tipo
*    O tipo se refere a uma estrutura de pilha, utilizada para armazenar o caminho
*	 para a saida do labirinto.
*
***********************************************************************/
typedef struct LAB_tagPilhaSolucao
{
	/* Indica se ha (1) ou nao (0) uma saida. */
	int saida;

	/* Tamanho maximo da pilha, calculado por uma funcao auxiliar. */
	int tamMaximo;

	/* Inteiro indicando o topo da pilha. */
	int topo;

	/* Inteiro que indica se a pilha esta cheia. */
	int cheia;

	/* Vetor do tipo eLAB_direcao contendo o caminho da entrada ate a saida. */
	eLAB_direcao *pCaminho;

} LAB_tpPilhaSolucao;

/* Protótipos das funções encapsuladas no módulo */

static eLAB_tpCelula LAB_VerificaCelula(LAB_tpLabirinto *pLabirinto, eLAB_direcao direcao);

static void LabirintoDefault(LAB_tpLabirinto *pLabirinto, MAT_tppMatriz pMatriz, int xTamanho, int yTamanho);

static void ArmazenaPosicaoAtual(LAB_tpLabirinto *pLabirinto, int *coordX, int *coordY);

static void RetornaPosicaoAtual(LAB_tpLabirinto *pLabirinto, int coordX, int coordY);

static void EncontraSolucao(LAB_tpLabirinto *pLabirinto, eLAB_direcao posAnterior, LAB_tpPilhaSolucao *pPilhaSolucao);

static void MarcaVisitado(LAB_tpLabirinto *pLabirinto);

static int VerificaVisitado(LAB_tpLabirinto *pLabirinto, eLAB_direcao posSeguinte, eLAB_direcao posAnterior);

static LAB_tpPilhaSolucao *InicializaPilhaSolucao(int tamMax);

static int PilhaVazia(LAB_tpPilhaSolucao *pPilhaSolucao);

static void InsereNaPilha(LAB_tpPilhaSolucao *pPilhaSolucao, eLAB_direcao direcao);

static eLAB_direcao RemoveDaPilha(LAB_tpPilhaSolucao *pPilhaSolucao);

static int GerarLimiteDaPilha(int labLinhas, int labColunas);

static void ImprimeSolucao(LAB_tpPilhaSolucao *pPilhaSolucao);

/* ********************************************* */

/* Variáveis globais encapsuladas no módulo
*
*	Descrição :
*		Variavéis que realizam a verificação para que não seja possivel sair da grid do Labirinto
*
*/

static int limiteVertical = 1;

static int limiteHorizontal = 1;

/* Descricao :
	Verifica se existe entrada e saida */

static int temEntrada = 0;

static int temSaida = 0;

/* ********************************************* */

/***************************************************************************
*
*  Função: LAB Criar Labirinto
*
***************************************************************************/
LAB_tpCondRet LAB_CriarLabirinto(LAB_tpLabirinto *pLabirinto, int xTamanho, int yTamanho)
{
	MAT_tpCondRet condRetMat;
	MAT_tppMatriz pMatriz;

	/* Verificação do tamanho da grid do Labirinto (não pode menor ou igual que 0 e nem maior que 10) */
	if (xTamanho > 10 || xTamanho <= 0 || yTamanho > 10 || yTamanho <= 0 || (xTamanho == 1 && yTamanho == 1))
	{
		return LAB_CondRetTamanhoInvalido;
	}

	temEntrada = temSaida = 0;
	limiteHorizontal = limiteVertical = 1;

	pMatriz = (MAT_tppMatriz)malloc(sizeof(MAT_tppMatriz));

	if (!pMatriz)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Verifica qual o maior valor passado, para que seja criada uma matriz base (grid) com tal valor. */
	condRetMat = xTamanho >= yTamanho ? MAT_CriarMatriz((MAT_tppMatriz *)pMatriz, xTamanho) : MAT_CriarMatriz((MAT_tppMatriz *)pMatriz, yTamanho);

	if (condRetMat == MAT_CondRetFaltouMemoria)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Popula Labirinto */
	LabirintoDefault(pLabirinto, pMatriz, xTamanho, yTamanho);

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Modificar Grid
*
***************************************************************************/
LAB_tpCondRet LAB_ModificarGrid(LAB_tpLabirinto *pLabirinto, int tipoModificacao)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	LAB_tpCelulaLabirinto *pCelula;
	LAB_tpCelulaLabirinto **pValor;

	/* Verifica se já existe o checkpoint */
	if ((temEntrada && (tipoModificacao == eLAB_Entrada)) || (temSaida && (tipoModificacao == eLAB_Saida)))
	{
		return LAB_CondRetTemCheckpoint;
	}

	pValor = (LAB_tpCelulaLabirinto **)malloc(sizeof(LAB_tpCelulaLabirinto));
	pCelula = (LAB_tpCelulaLabirinto *)malloc(sizeof(LAB_tpCelulaLabirinto));
	if (!pCelula || !pValor)
	{
		return LAB_CondRetFaltouMemoria;
	}

	matCondRet = MAT_ObterValorCorr((MAT_tppMatriz *)pLabirinto->pMatrizBase, (void **)&pValor);

	/* Se o valor corrente e entrada ou saida e o tipo de
	modificacao e caminho ou parede, os indicadores de entrada e saida falsificam */
	if (matCondRet != MAT_CondRetValNoNull)
	{
		if ((*pValor)->tpCelula == eLAB_Entrada)
		{
			temEntrada = 0;
		}
		else if ((*pValor)->tpCelula == eLAB_Saida)
		{
			temSaida = 0;
		}
	}

	/* Verifica se o labirinto e grid existem (não são nulos) */
	labCondRet = !pLabirinto ? LAB_CondRetLabirintoNaoExiste : !pLabirinto->pMatrizBase ? LAB_CondRetLabirintoNaoExiste : LAB_CondRetOK;

	if (labCondRet != LAB_CondRetOK)
	{
		return labCondRet;
	}

	switch (tipoModificacao)
	{
	case eLAB_Entrada:
		temEntrada = 1;
		pCelula->tpCelula = eLAB_Entrada;
		pCelula->visitado = 0;
		break;

	case eLAB_Saida:
		temSaida = 1;
		pCelula->tpCelula = eLAB_Saida;
		pCelula->visitado = 0;
		break;

	case eLAB_Caminho:
		pCelula->tpCelula = eLAB_Caminho;
		pCelula->visitado = 0;
		break;

	case eLAB_Parede:
		pCelula = NULL;
		break;
	}

	/* Insere nó na Grid (Matriz) */
	matCondRet = MAT_InsereValor((MAT_tppMatriz *)pLabirinto->pMatrizBase, pCelula);

	if (matCondRet != MAT_CondRetOK)
	{
		return LAB_CondRetErroNaAcao;
	}

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Andar pelo Labirinto
*
***************************************************************************/
LAB_tpCondRet LAB_AndarPeloLab(LAB_tpLabirinto *pLabirinto, eLAB_direcao direcao, int tipoMovimentacao)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	eLAB_tpCelula tpCelulaSeguinte;

	/* Verifica se o labirinto e grid existem (não são nulos) */
	labCondRet = !pLabirinto ? LAB_CondRetLabirintoNaoExiste : !pLabirinto->pMatrizBase ? LAB_CondRetLabirintoNaoExiste : LAB_CondRetOK;

	if (labCondRet != LAB_CondRetOK)
	{
		return labCondRet;
	}

	/* Verifica se o jogador não está movimentando além dos limites do labirinto */
	switch (direcao)
	{
	case eLAB_direita:

		if ((limiteHorizontal + 1) > pLabirinto->LAB_colunas)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteHorizontal += 1;
		}

		break;

	case eLAB_esquerda:

		if ((limiteHorizontal - 1) < 1)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteHorizontal -= 1;
		}

		break;

	case eLAB_cima:

		if ((limiteVertical - 1) < 1)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteVertical -= 1;
		}

		break;

	case eLAB_baixo:

		if ((limiteVertical + 1) > pLabirinto->LAB_linhas)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteVertical += 1;
		}
	}

	tpCelulaSeguinte = LAB_VerificaCelula(pLabirinto, direcao);

	if (tpCelulaSeguinte == eLAB_Invalido)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Caso seja uma movimentação restrita (não se pode passar por paredes) */
	if (tipoMovimentacao == eLAB_MovRestrito)
	{
		if (tpCelulaSeguinte == eLAB_Parede)
		{
			return LAB_CondRetEhParede;
		}

		MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, direcao);

		if (tpCelulaSeguinte == eLAB_Saida)
		{
			return LAB_CondRetFimLabirinto;
		}

		else
		{
			return LAB_CondRetOK;
		}
	}

	/* Caso seja movimentação livre (pode-se passar por paredes, para facilitar a inserção das mesmas) */
	matCondRet = MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, direcao);

	if (matCondRet == MAT_CondRetNaoPossuiAdjacencia)
	{
		return LAB_CondRetMovimentacaoInvalida;
	}

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Mostra Labirinto
*
***************************************************************************/
LAB_tpCondRet LAB_MostraLabirinto(LAB_tpLabirinto *pLabirinto, int linha, int coluna)
{
	int colunaX = 0, linhaY = 0;
	int direcao = eLAB_esquerda;
	int i, j, tipo, check;
	LAB_tpCelulaLabirinto **pValor;

	pValor = (LAB_tpCelulaLabirinto **)malloc(sizeof(LAB_tpCelulaLabirinto));

	if (!pLabirinto)
	{
		return LAB_CondRetLabirintoNaoExiste;
	}

	if (!pValor)
	{
		return LAB_CondRetFaltouMemoria;
	}

	ArmazenaPosicaoAtual(pLabirinto, &linhaY, &colunaX);
	printf("\n");

	for (i = 0; i < pLabirinto->LAB_linhas; i++)
	{
		for (j = 0; j < pLabirinto->LAB_colunas; j++)
		{
			tipo = MAT_ObterValorCorr((MAT_tppMatriz *)pLabirinto->pMatrizBase, (void **)&pValor);
			if (i == linha && j == coluna)
			{
				check = 1;
			}
			else
			{
				check = 0;
			}

			if (tipo != MAT_CondRetValNoNull)
			{
				switch ((*pValor)->tpCelula)
				{
				case eLAB_Caminho:
					if (check)
					{
						printf("   O\t");
					}
					else
					{
						printf("   \t");
					}
					break;

				case eLAB_Entrada:
					if (check)
					{
						printf("  O E\t");
					}
					else
					{
						printf("   E\t");
					}
					break;

				case eLAB_Saida:
					if (check)
					{
						printf("  O S\t");
					}
					else
					{
						printf("   S\t");
					}
					break;

				default:
					break;
				}
			}
			else
			{
				if (check)
				{
					printf("  O X\t");
				}
				else
				{
					printf("   X\t");
				}
			}

			MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_direita);
		}

		/*
		o Labirinto é impresso que nem uma maquina de escrever, quando chega
		no final volta para o comeco e desce a linha
		 */
		while (j > 0 && i != (pLabirinto->LAB_linhas - 1))
		{
			MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_esquerda);
			j--;
		};

		MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_baixo);
		printf("|\n\n");
	}

	RetornaPosicaoAtual(pLabirinto, linhaY, colunaX);

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Verifica CheckPoints
*
***************************************************************************/
LAB_tpCondRet LAB_VerificaCheckpoints(LAB_tpLabirinto *pLabirinto, int opt)
{
	eLAB_tpCelula tipo;
	if (!pLabirinto)
	{
		return LAB_CondRetLabirintoNaoExiste;
	}

	if (opt)
	{
		if (temSaida && temEntrada)
		{
			return LAB_CondRetOK;
		}
	}
	else
	{
		tipo = LAB_VerificaCelula(pLabirinto, eLAB_Corrente);
		if (tipo != eLAB_Entrada)
		{
			return LAB_CondRetErroNaAcao;
		}
		return LAB_CondRetOK;
	}

	return LAB_CondRetErroNaAcao;
}

/***************************************************************************
*
*  Função: LAB Excluir Labirinto
*
***************************************************************************/
LAB_tpCondRet LAB_ExcluiLabirinto(LAB_tpLabirinto *pLabirinto)
{

	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	int tamMatriz;

	/* Verifica se o labirinto e grid existem (não são nulos) */
	labCondRet = !pLabirinto ? LAB_CondRetLabirintoNaoExiste : !pLabirinto->pMatrizBase ? LAB_CondRetLabirintoNaoExiste : LAB_CondRetOK;

	if (labCondRet != LAB_CondRetOK)
	{
		return labCondRet;
	}

	if (pLabirinto->LAB_colunas >= pLabirinto->LAB_linhas)
	{
		tamMatriz = pLabirinto->LAB_colunas;
	}

	else
	{
		tamMatriz = pLabirinto->LAB_linhas;
	}

	matCondRet = MAT_EsvaziaMatriz((MAT_tppMatriz *)pLabirinto->pMatrizBase, tamMatriz);

	switch (matCondRet)
	{
	case MAT_CondRetOK:
		return LAB_CondRetOK;
		break;

	case MAT_CondRetMatrizVazia:
		return LAB_CondRetLabirintoNaoExiste;
		break;
	}

	matCondRet = MAT_DestroiMatriz((MAT_tppMatriz *)pLabirinto->pMatrizBase, tamMatriz);
	free(pLabirinto);
	pLabirinto = NULL;

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Desvendar Labirinto
*
***************************************************************************/
LAB_tpCondRet LAB_DesvendaLabirinto(LAB_tpLabirinto *pLabirinto)
{
	int tamMax = GerarLimiteDaPilha(pLabirinto->LAB_linhas, pLabirinto->LAB_colunas);
	int linhaUsu, colunaUsu;
	LAB_tpPilhaSolucao *pPilhaSolucao = InicializaPilhaSolucao(tamMax);
	if (!pPilhaSolucao)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Armazena a posicao atual para uso posterior, logo apos volta para conclusao */
	ArmazenaPosicaoAtual(pLabirinto, &linhaUsu, &colunaUsu);
	RetornaPosicaoAtual(pLabirinto, linhaUsu, colunaUsu);

	EncontraSolucao(pLabirinto, eLAB_Corrente, pPilhaSolucao);

	/* ResetaVisitados altera a posicao do usuario, sendo assim "retornaPosicaoAtual" volta para continuacao do usuario */
	RetornaPosicaoAtual(pLabirinto, linhaUsu, colunaUsu);

	/* Verifica se a pilha esta vazia (nao existe solucao). */
	if (PilhaVazia(pPilhaSolucao) == 1)
	{
		puts("Sem solucao.");
		return LAB_CondRetLabSemSolucao;
	}

	/* Verifica se ela esta cheia (realizaram-se muitas verificacoes sem sucesso) */
	if (pPilhaSolucao->cheia == 1)
	{
		return LAB_CondRetLabSemSolucao;
	}

	/* Imprimir a Pilha */
	ImprimeSolucao(pPilhaSolucao);

	return LAB_CondRetOK;
}

/* Código das funções encapsuladas no módulo */

/***********************************************************************
*
*  $FC Funcao: LAB Verificar Célula
*
*  $FV Valor retornado
*     Inteiro indicando o tipo do nó para o qual deseja-se
*	  realizar o movimento.
*
***********************************************************************/
eLAB_tpCelula LAB_VerificaCelula(LAB_tpLabirinto *pLabirinto, eLAB_direcao direcao)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCelulaLabirinto **pValor;
	eLAB_direcao movBack;

	pValor = (LAB_tpCelulaLabirinto **)malloc(sizeof(LAB_tpCelulaLabirinto));

	if (!pValor)
	{
		return eLAB_Invalido;
	}

	if (direcao != eLAB_Corrente)
	{

		/* Ida */
		matCondRet = MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, direcao);

		if (matCondRet == MAT_CondRetNaoPossuiAdjacencia)
		{
			return eLAB_Parede;
		}

		/* Obtém o tipo do nó do grid. */
		matCondRet = MAT_ObterValorCorr((MAT_tppMatriz *)pLabirinto->pMatrizBase, (void **)&pValor);

		switch (direcao)
		{
		case eLAB_direita:
			movBack = eLAB_esquerda;
			break;
		case eLAB_cima:
			movBack = eLAB_baixo;
			break;
		case eLAB_baixo:
			movBack = eLAB_cima;
			break;
		case eLAB_esquerda:
			movBack = eLAB_direita;
			break;
		}

		/* Volta */
		MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, movBack);
	}
	else
	{
		matCondRet = MAT_ObterValorCorr((MAT_tppMatriz *)pLabirinto->pMatrizBase, (void **)&pValor);
	}

	if (matCondRet == MAT_CondRetValNoNull)
	{
		return eLAB_Parede;
	}

	return (*pValor)->tpCelula;
}

/***********************************************************************
*
*  $FC Funcao: Labirinto Default - LAB Criar Labirinto (auxiliar)
*
*  $FV Valor retornado
*     Sem retorno. Função auxiliar na
*	  criação de um labirinto.
*
***********************************************************************/
void LabirintoDefault(LAB_tpLabirinto *pLabirinto, MAT_tppMatriz pMatriz, int xTamanho, int yTamanho)
{
	pLabirinto->pMatrizBase = pMatriz;
	pLabirinto->LAB_colunas = yTamanho;
	pLabirinto->LAB_linhas = xTamanho;
}

/***********************************************************************
*
*  	$FC Funcao: Armazena indiceVetor original
*
***********************************************************************/
void ArmazenaPosicaoAtual(LAB_tpLabirinto *pLabirinto, int *linha, int *coluna)
{
	int colunas = 0, linhas = 0;

	while (MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_esquerda) != MAT_CondRetNaoPossuiAdjacencia)
	{
		colunas++;
	}
	while (MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_cima) != MAT_CondRetNaoPossuiAdjacencia)
	{
		linhas++;
	}
	*coluna = colunas;
	*linha = linhas;

	return;
}

/***********************************************************************
*
*  	$FC Funcao: Retorna indiceVetor original
*
***********************************************************************/
void RetornaPosicaoAtual(LAB_tpLabirinto *pLabirinto, int linha, int colunas)
{
	/* Volta para nó cabeça */
	MAT_DestroiMatriz((MAT_tppMatriz *)pLabirinto->pMatrizBase, 0);

	/* Volta usuario para o lugar certo */
	while (linha != 0)
	{
		MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_baixo);
		linha--;
	}
	while (colunas != 0)
	{
		MAT_IrParaNo((MAT_tppMatriz *)pLabirinto->pMatrizBase, eLAB_direita);
		colunas--;
	}

	return;
}

/***********************************************************************
*
*  	$FC Funcao: Encontra Solucao
*
*	$ED Descrição
*		Vasculha o labirinto em busca de um caminho que leve o usuario
*		da entrada ate a saida do mesmo. Assume-se que o No Corrente
*		encontra-se na entrada.
*
***********************************************************************/
void EncontraSolucao(LAB_tpLabirinto *pLabirinto, eLAB_direcao posAnterior, LAB_tpPilhaSolucao *pPilhaSolucao)
{
	/* Caso seja encontrada a saida, pPilhaSolucao->saida = 1 para que nao modifique a pilha de solucao. */
	if (LAB_VerificaCelula(pLabirinto, eLAB_Corrente) == eLAB_Saida)
	{
		pPilhaSolucao->saida = 1;
		return;
	}

	if (pPilhaSolucao->saida != 1)
	{
		if (LAB_VerificaCelula(pLabirinto, eLAB_direita) != eLAB_Parede)
		{
			if (posAnterior != eLAB_direita && VerificaVisitado(pLabirinto, eLAB_direita, eLAB_esquerda) == 0)
			{
				LAB_AndarPeloLab(pLabirinto, eLAB_direita, eLAB_MovRestrito);
				MarcaVisitado(pLabirinto);
				InsereNaPilha(pPilhaSolucao, eLAB_direita);
				EncontraSolucao(pLabirinto, eLAB_esquerda, pPilhaSolucao);
			}
		}
	}

	if (pPilhaSolucao->saida != 1)
	{
		if (LAB_VerificaCelula(pLabirinto, eLAB_cima) != eLAB_Parede)
		{
			if (posAnterior != eLAB_cima && VerificaVisitado(pLabirinto, eLAB_cima, eLAB_baixo) == 0)
			{
				LAB_AndarPeloLab(pLabirinto, eLAB_cima, eLAB_MovRestrito);
				MarcaVisitado(pLabirinto);
				InsereNaPilha(pPilhaSolucao, eLAB_cima);
				EncontraSolucao(pLabirinto, eLAB_baixo, pPilhaSolucao);
			}
		}
	}

	if (pPilhaSolucao->saida != 1)
	{
		if (LAB_VerificaCelula(pLabirinto, eLAB_esquerda) != eLAB_Parede)
		{
			if (posAnterior != eLAB_esquerda && VerificaVisitado(pLabirinto, eLAB_esquerda, eLAB_direita) == 0)
			{
				LAB_AndarPeloLab(pLabirinto, eLAB_esquerda, eLAB_MovRestrito);
				MarcaVisitado(pLabirinto);
				InsereNaPilha(pPilhaSolucao, eLAB_esquerda);
				EncontraSolucao(pLabirinto, eLAB_direita, pPilhaSolucao);
			}
		}
	}

	if (pPilhaSolucao->saida != 1)
	{
		if (LAB_VerificaCelula(pLabirinto, eLAB_baixo) != eLAB_Parede)
		{
			if (posAnterior != eLAB_baixo && VerificaVisitado(pLabirinto, eLAB_baixo, eLAB_cima) == 0)
			{
				LAB_AndarPeloLab(pLabirinto, eLAB_baixo, eLAB_MovRestrito);
				MarcaVisitado(pLabirinto);
				InsereNaPilha(pPilhaSolucao, eLAB_baixo);
				EncontraSolucao(pLabirinto, eLAB_cima, pPilhaSolucao);
			}
		}
	}

	/* Caso tenha encontrado a saida, retorna sem alterar a pilha. */
	if (pPilhaSolucao->saida == 1)
	{
		return;
	}

	/* Se a saida nao foi encontrada, retorna a indiceVetor anterior e realiza um pop na pilha. */
	else
	{
		LAB_AndarPeloLab(pLabirinto, posAnterior, eLAB_MovRestrito);
		RemoveDaPilha(pPilhaSolucao);
		return;
	}
}

/***********************************************************************
*
*  	$FC Funcao: Marca Visitado
*
*	$ED Descrição
*		Funcao que, ao passar por um no, modifica um no do grid para
*		marca-lo como "visitado", auxiliando o desvendador.
*
***********************************************************************/
void MarcaVisitado(LAB_tpLabirinto *pLabirinto)
{
	LAB_tpCelulaLabirinto *pCelula;
	MAT_tpCondRet matCondRet;
	eLAB_tpCelula celulaAtual = LAB_VerificaCelula(pLabirinto, eLAB_Corrente);

	pCelula = (LAB_tpCelulaLabirinto *)malloc(sizeof(LAB_tpCelulaLabirinto));
	if (!pCelula)
	{
		return;
	}

	/* Muda a flag visitado do no atual para auxiliar o desvendador. */
	pCelula->visitado = 1;
	pCelula->tpCelula = celulaAtual;
	matCondRet = MAT_InsereValor((MAT_tppMatriz *)pLabirinto->pMatrizBase, pCelula);
}

/***********************************************************************
*
*  	$FC Funcao: Verifica Visitado
*
*	$ED Descrição
*		Funcao que verifica se o no ao qual deseja-se mover
*		ja foi visitado ou nao.
*
*  	$FV Valor retornado
*		Inteiro indicando se o no ja foi visitado (1) ou nao (0).
*
***********************************************************************/
int VerificaVisitado(LAB_tpLabirinto *pLabirinto, eLAB_direcao posSeguinte, eLAB_direcao posAnterior)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	LAB_tpCelulaLabirinto **pValor;

	pValor = (LAB_tpCelulaLabirinto **)malloc(sizeof(LAB_tpCelulaLabirinto));
	if (!pValor)
	{
		return -1;
	}

	labCondRet = LAB_AndarPeloLab(pLabirinto, posSeguinte, eLAB_MovRestrito);

	/* Caso seja um caminho ou saida */
	if (labCondRet == LAB_CondRetOK || labCondRet == LAB_CondRetFimLabirinto)
	{
		matCondRet = MAT_ObterValorCorr((MAT_tppMatriz *)pLabirinto->pMatrizBase, (void **)&pValor);
		if ((*pValor)->visitado == 1)
		{
			labCondRet = LAB_AndarPeloLab(pLabirinto, posAnterior, eLAB_MovRestrito);
			return 1;
		}

		else
		{
			labCondRet = LAB_AndarPeloLab(pLabirinto, posAnterior, eLAB_MovRestrito);
			return 0;
		}
	}

	return -1;
}

/***********************************************************************
*
*  	$FC Funcao: Inicializa a Pilha de Solucao
*
*	$ED Descrição
*		Funcao auxiliar criada para inicializar uma variavel do tipo LAB_tpPilhaSolucao
*
*  	$FV Valor retornado
*		LAB_tpPilhaSolucao contento o tipo ja inicializado.
*
***********************************************************************/
LAB_tpPilhaSolucao *InicializaPilhaSolucao(int tamMax)
{
	LAB_tpPilhaSolucao *pPilhaSolucao = (LAB_tpPilhaSolucao *)malloc(sizeof(LAB_tpPilhaSolucao));
	if (!pPilhaSolucao)
	{
		return NULL;
	}

	pPilhaSolucao->saida = 0;
	pPilhaSolucao->tamMaximo = tamMax;
	pPilhaSolucao->topo = -1;
	pPilhaSolucao->cheia = 0;
	pPilhaSolucao->pCaminho = (eLAB_direcao *)malloc(sizeof(eLAB_direcao) * tamMax);

	return pPilhaSolucao;
}

/***********************************************************************
*
*  	$FC Funcao: Verifica Pilha Vazia
*
*	$ED Descrição
*		Funcao auxiliar criada para verificar se a pilha esta vazia ou nao.
*
*  	$FV Valor retornado
*		Inteiro indicando se a pilha esta vazia ou nao.
*
***********************************************************************/
int PilhaVazia(LAB_tpPilhaSolucao *pPilhaSolucao)
{
	return pPilhaSolucao->topo <= -1;
}

/***********************************************************************
*
*  	$FC Funcao: Insere na Pilha
*
*	$ED Descrição
*		Funcao que insere um caminho na pilha.
*
***********************************************************************/
void InsereNaPilha(LAB_tpPilhaSolucao *pPilhaSolucao, eLAB_direcao direcao)
{
	/* Caso a pilha atinja seu limite, assume que nao ha solucao. */
	if (pPilhaSolucao->topo == pPilhaSolucao->tamMaximo - 1)
	{
		pPilhaSolucao->cheia = 1;
		return;
	}

	pPilhaSolucao->pCaminho[++pPilhaSolucao->topo] = direcao;
}

/***********************************************************************
*
*  	$FC Funcao: Remove um valor da Pilha
*
*	$ED Descrição
*		Funcao auxiliar criada para remover o ultimo valor inserido na pilha.
*
*  	$FV Valor retornado
*		Tipo eLAB_direcao contendo a ultima direcao tomada para chegar a saida do labirinto.
*
***********************************************************************/
eLAB_direcao RemoveDaPilha(LAB_tpPilhaSolucao *pPilhaSolucao)
{
	return pPilhaSolucao->pCaminho[pPilhaSolucao->topo--];
}

/***********************************************************************
*
*  	$FC Funcao: Gera um Limite para a Pilha
*
*	$ED Descrição
*		Funcao auxiliar criada para arbitrar um tamanho maximo para a pilha de solucao
*		baseado nas dimensoes do labirinto.
*
*  	$FV Valor retornado
*		Inteiro contendo o limite maximo da pilha.
*
***********************************************************************/
int GerarLimiteDaPilha(int labLinhas, int labColunas)
{
	return labColunas * labLinhas;
}

/***********************************************************************
*
*  	$FC Funcao: Imprime Solucao
*
*	$ED Descrição
*		Funcao auxiliar criada para imprimir o caminho da entrada ate a saida.
*
***********************************************************************/
void ImprimeSolucao(LAB_tpPilhaSolucao *pPilhaSolucao)
{
	int i;
	eLAB_direcao topoPilha;
	eLAB_direcao *pPilhaInvertida;
	int indiceVetor = pPilhaSolucao->topo;
	int totalPassos = pPilhaSolucao->topo;

	pPilhaInvertida = (eLAB_direcao *)malloc(sizeof(eLAB_direcao) * pPilhaSolucao->topo);
	if (!pPilhaInvertida)
	{
		return;
	}

	for (i = pPilhaSolucao->topo; i >= 0; i--)
	{
		topoPilha = RemoveDaPilha(pPilhaSolucao);
		pPilhaInvertida[indiceVetor--] = topoPilha;
	}

	puts("Solucao do labirinto:");

	for (i = 0; i <= totalPassos; i++)
	{
		switch (pPilhaInvertida[i])
		{
		case eLAB_direita:
			puts("- Direita");
			break;
		case eLAB_esquerda:
			puts("- Esquerda");
			break;
		case eLAB_baixo:
			puts("- Baixo");
			break;
		default:
			puts("- Cima");
			break;
		}
	}
}
