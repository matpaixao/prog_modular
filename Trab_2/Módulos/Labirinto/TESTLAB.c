﻿/***************************************************************************
*  $MCI Módulo de implementação: Módulo de teste específico
*
*  Arquivo gerado:              testlab.c
*  Letras identificadoras:      TLAB
*
*  Projeto: Disciplina INF1301
*  Autor:  fac - Fernanda de Almeida Castro
*
*  $HA Históico de evolução:
*     Versão   Autor    Data         Observações
*    1.0.0         fac      24/09/2019    Inicio do desenvolvimento.
*    2.0.0         fac      30/09/2019    Revisão de casos de teste e adição de casos de teste
*    2.1.0         fac      01/10/2019    Testes revisados
*
*
***************************************************************************/

#include<string.h>
#include<stdio.h>
#include<malloc.h>

#include"tst_espc.h"
#include"lerparm.h"
#include"generico.h"
#include"labirinto.h"

#define CRIAR_LAB_CMD "=criar"
#define MODIFICAR_GRID_CMD "=modificar"
#define ANDAR_LAB_CMD "=andar"
#define EXCLUI_LAB_CMD "=exclui"

#define DIM_VT_LAB 10

LAB_tppLabirinto pLabirinto;

/***********************************************************************
*
*  $FC FunÁ„o: TLAB&Testar Labirinto
*
*  $ED DescriÁ„o da funÁ„o
*     Podem ser criadas atÈ 10 labirintos, identificadas pelos Ìndices 0 a 9.
*
*     Comandos disponÌveis:
*
*     =criar                     indiceLab   tamanhox   tamanhoy      CondRetEsp
*     =modificar                 indiceLab   tipoMod    CondRetEsp
*     =andar                     indiceLab   direcao    CondRetEsp
*     =exclui                    indiceLab   CondRetEsp
*
***********************************************************************/


TST_tpCondRet TST_EfetuarComando(char * ComandoTeste)
{
    int numLidos = -1;
    int indiceLab = -1;

    /* Varios casos necessitam do tamanho do labirinto. */
    int tamLabx = -1;
    int tamLaby = -1;

    /* DireÁ„o de movimentaÁ„o no labirinto. */
    int dir = -1;
    
    
    /* Tipo da modificacao no labirinto*/
    int tipomod = -1;
    int tipomov = -1;

    /* InicializaÁ„o das condiÁıes de retorno. */
    LAB_tpCondRet CondRetEsperado = LAB_CondRetOK;
    LAB_tpCondRet CondRetObtido = LAB_CondRetFaltouMemoria;


    LAB_tppLabirinto pLab = (LAB_tppLabirinto)malloc(sizeof(LAB_tppLabirinto*));

    /* Criar Labirinto */
    if (strcmp(ComandoTeste, CRIAR_LAB_CMD) == 0)
    {
        numLidos = LER_LerParametros("iiii", &indiceLab, &tamLabx, &tamLaby, &CondRetEsperado);
        
        if (numLidos != 4)
        {
            return TST_CondRetParm;
        }
        CondRetObtido = LAB_CriarLabirinto(pLab, tamLabx,tamLaby);

        pLabirinto = pLab;

        return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao criar a Labirinto.");
    }

    /* Modificar grid do labirinto */
    else if (strcmp(ComandoTeste, MODIFICAR_GRID_CMD) == 0)
    {
        numLidos = LER_LerParametros("iii", &indiceLab, &tipomod, &CondRetEsperado);

        if (numLidos != 3)
        {
            return TST_CondRetParm;
        }

        CondRetObtido = LAB_ModificarGrid(pLabirinto, tipomod);

        return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao modificar no o labirinto.");
    }
    
    /* Andar pelo labirinto  */
    else if (strcmp(ComandoTeste, ANDAR_LAB_CMD) == 0)
    {
        numLidos = LER_LerParametros("iiii", &indiceLab, &dir, &tipomov, &CondRetEsperado);
        
        if (numLidos != 4)
        {
            return TST_CondRetParm;
        }

        CondRetObtido = LAB_AndarPeloLab(pLabirinto, dir, tipomov);
        
        return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao deslocar no labirinto.");
    }
    
    /* Excluir labirinto */
    else if (strcmp(ComandoTeste, EXCLUI_LAB_CMD) == 0)
    {
        numLidos = LER_LerParametros("ii", &indiceLab, &CondRetEsperado);

        if (numLidos != 2)
        {
            return TST_CondRetParm;
        }

        CondRetObtido = LAB_ExcluiLabirinto(pLabirinto);

        return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao excluir labirinto.");
    }
    
        return TST_CondRetNaoConhec;
}
