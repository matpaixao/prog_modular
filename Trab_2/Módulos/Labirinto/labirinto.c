﻿
/***************************************************************************
*  $MCI Módulo de implementação: Módulo labirinto
*
*  Arquivo gerado:              LABIRINTO.C
*  Letras identificadoras:      LAB
*
*  Projeto: Disciplina INF1301
*  Gestor:  DI/PUC-Rio
*  Autores: 
*		glg - Gulherme de Lacerda Gomes
*       mrp - Matheus Romero Paixão
*       fac - Fernanda de Almeida Castro
*
*  $HA Hist�rico de evolu��o:
*     Vers�o  Autor   	 Data     	Observa��es
*       1.0.0    glg   	18/09/2019 	In�cio do desenvolvimento
*		1.1.0	 glg   	22/09/2019 	Adi��o de mais fun��es ao m�dulo.
*		2.0.0	 glg   	26/09/2019 	Refatora��o completa do m�dulo labrinto.
*		2.0.1    mrp   	27/09/2019	Reorganização do Módulo
*		2.1.0	 mrp	29/09/2019	Adição e refatorações de funções 
*		2.2.0    glg    30/09/2019  Implementação das últimas funcionalidades e revisão do módulo.
*		2.2.1    mrp	01/10/2019  Hotfixes 
*		2.2.2    glg    01/10/2019  Revisão de código
*		2.3.0    glg    02/10/2019  Correção da função VerificaCelula e adição de verificação dos limites do labirinto.
*       2.3.1    fac    03/10/2019  Revisão de código
*		2.3.2	 mrp	04/10/2019	Revisão de código
*		2.3.3	 mrp	06/10/2019	Revisão de código
*		2.3.4	 glg	06/10/2019	Revisão de código
*
***************************************************************************/

#include "matriz.h"

#define LABIRINTO_OWN
#include "labirinto.h"
#undef LABIRINTO_OWN

#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor da direção de movimento
*
*
*  $ED Descrição do tipo
*    Cada inteiro representa uma direção desejada de locomoção,
*	o tipo corrente foi criado a fim de auxiliar na função verifica
*	célula, que no caso, se refere a própria célula
*
***********************************************************************/

typedef enum
{
	eLAB_Corrente = -1,
	eLAB_cima = 5,
	eLAB_baixo = 6,
	eLAB_direita = 8,
	eLAB_esquerda = 7

} eLAB_direcao;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor do tipo de célula.
*
*
*  $ED Descrição do tipo
*    Cada inteiro representa um tipo de célula. O tipo inválido foi criado a fim
*	 de auxiliar no retorno da função VerificaCelula, para o caso da célula não
*	 fazer parte do grid.
*
***********************************************************************/

typedef enum
{
	eLAB_Parede = 0,
	eLAB_Entrada = 1,
	eLAB_Saida = 2,
	eLAB_Caminho = 3,
	eLAB_Invalido = 4

} eLAB_tpCelula;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor do tipo de movimentação
*
*
*  $ED Descrição do tipo
*    Cada inteiro representa um tipo de movimentação. O tipo livre permite a locomoção em
*	 qualquer direção (não somente Cima, Baixo, Esquerda, Direita), e também permite "andar"
*	 por paredes. O mesmo foi feito para facilitar a criação do labirinto.
*	 Já o tipo restrito é o tipo de movimento do usuário durante a movimentação pelo
*	 labirinto já construído.
*
***********************************************************************/

typedef enum
{
	eLAB_MovLivre = 0,
	eLAB_MovRestrito = 1

} eLAB_tpMovimentacao;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor da cabe�a de um labirinto.
*
*
*  $ED Descri��o do tipo
*     O tipo refere-se � cabe�a de um labirinto, contendo um ponteiro para sua
*	  matriz base (matriz que representa as paredes, caminhos, entrada e sa�da),
*	  as coordenadas de entrada e saida e o tamanho do labirinto.
*
***********************************************************************/

typedef struct LAB_tagLabirinto
{
	/*  Ponteiro para uma matriz que cont�m
	 *  armazenado os caminhos, paredes, 
	 *  entrada e sa�da do labirinto.
	 */
	MAT_tppMatriz pMatrizBase;

	/* Inteiro referente ao número de linhas */
	int LAB_linhas;

	/* Inteiro referente ao número de colunas */
	int LAB_colunas;

} LAB_tpLabirinto;

/***********************************************************************
*
*  $TC Tipo de dados: LAB Descritor de uma célula do labirinto
*
*
*  $ED Descrição do tipo
*    Inteiro que representa o tipo da célula (Parede, Entrada, Saída, Caminho).
*
***********************************************************************/

typedef struct LAB_tagCelulaLabirinto
{
	/* Contém o tipo de célula do grid (parede, entrada, saída ou caminho) */
	eLAB_tpCelula tpCelula;

} LAB_tpCelulaLabirinto;

/* Protótipos das funções encapsuladas no módulo */

eLAB_tpCelula LAB_VerificaCelula(LAB_tpLabirinto *pLabirinto, int direcao);

void LabirintoDefault(LAB_tpLabirinto *pLabirinto, MAT_tppMatriz pMatriz, int xTamanho, int yTamanho);

/* ********************************************* */

/* Variáveis globais encapsuladas no módulo 
*
*	Descrição :
*		Variavéis que realizam a verificação para que não seja possivel sair da grid do Labirinto
*
*/

static unsigned int limiteVertical = 1;

static unsigned int limiteHorizontal = 1;

/* ********************************************* */

/***************************************************************************
*
*  Função: LAB Criar Labirinto
*
***************************************************************************/

LAB_tpCondRet LAB_CriarLabirinto(LAB_tpLabirinto *pLabirinto, int xTamanho, int yTamanho)
{
	MAT_tpCondRet condRetMat;
	MAT_tppMatriz pMatriz;

	/* Verificação do tamanho da grid do Labirinto (não pode menor ou igual que 0 e nem maior que 10) */
	if (xTamanho > 10 || xTamanho <= 0 || yTamanho > 10 || yTamanho <= 0 || (xTamanho == 1 && yTamanho == 1))
	{
		return LAB_CondRetTamanhoInvalido;
	}

	pMatriz = (MAT_tppMatriz)malloc(sizeof(MAT_tppMatriz));

	if (!pMatriz)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Verifica qual o maior valor passado, para que seja criada uma matriz base (grid) com tal valor. */
	condRetMat = xTamanho >= yTamanho ? MAT_CriarMatriz(pMatriz, xTamanho) : MAT_CriarMatriz(pMatriz, yTamanho);

	if (condRetMat == MAT_CondRetFaltouMemoria)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Popula Labirinto */
	LabirintoDefault(pLabirinto, pMatriz, xTamanho, yTamanho);

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Modificar Grid
*
***************************************************************************/

LAB_tpCondRet LAB_ModificarGrid(LAB_tpLabirinto *pLabirinto, int tipoModificacao)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	LAB_tpCelulaLabirinto *pCelula;

	pCelula = (LAB_tpCelulaLabirinto *)malloc(sizeof(LAB_tpCelulaLabirinto));

	if (!pCelula)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Verifica se o labirinto e grid existem (não são nulos) */
	labCondRet = !pLabirinto ? LAB_CondRetLabirintoNaoExiste : !pLabirinto->pMatrizBase ? LAB_CondRetLabirintoNaoExiste : LAB_CondRetOK;

	if (labCondRet != LAB_CondRetOK)
	{
		return labCondRet;
	}

	switch (tipoModificacao)
	{
	case eLAB_Entrada:
		pCelula->tpCelula = eLAB_Entrada;
		break;

	case eLAB_Saida:
		pCelula->tpCelula = eLAB_Saida;
		break;

	case eLAB_Caminho:
		pCelula->tpCelula = eLAB_Caminho;
		break;

	case eLAB_Parede:
		pCelula = NULL;
		break;
	}

	matCondRet = MAT_InsereValor(pLabirinto->pMatrizBase, pCelula);

	if (matCondRet != MAT_CondRetOK)
	{
		return LAB_CondRetErroNaAcao;
	}

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Andar pelo Labirinto
*
***************************************************************************/

LAB_tpCondRet LAB_AndarPeloLab(LAB_tpLabirinto *pLabirinto, int direcao, int tipoMovimentacao)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	eLAB_tpCelula tpCelulaSeguinte;

	/* Verifica se o labirinto e grid existem (não são nulos) */
	labCondRet = !pLabirinto ? LAB_CondRetLabirintoNaoExiste : !pLabirinto->pMatrizBase ? LAB_CondRetLabirintoNaoExiste : LAB_CondRetOK;

	if (labCondRet != LAB_CondRetOK)
	{
		return labCondRet;
	}

	/* Verifica se o jogador não etá movimentando além dos limites do labirinto */
	switch (direcao)
	{
	case eLAB_direita:

		if ((limiteHorizontal + 1) > pLabirinto->LAB_colunas)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteHorizontal += 1;
		}

		break;

	case eLAB_esquerda:

		if ((limiteHorizontal - 1) < 1)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteHorizontal -= 1;
		}

		break;

	case eLAB_cima:

		if ((limiteVertical - 1) < 1)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteVertical -= 1;
		}

		break;

	case eLAB_baixo:

		if ((limiteVertical + 1) > pLabirinto->LAB_linhas)
		{
			if (tipoMovimentacao == eLAB_MovLivre)
			{
				return LAB_CondRetAcaoInvalida;
			}
			else
			{
				return LAB_CondRetEhParede;
			}
		}

		else
		{
			limiteVertical += 1;
		}
	}

	tpCelulaSeguinte = LAB_VerificaCelula(pLabirinto, direcao);

	if (tpCelulaSeguinte == eLAB_Invalido)
	{
		return LAB_CondRetFaltouMemoria;
	}

	/* Caso seja uma movimentação restrita (não se pode passar por paredes) */
	if (tipoMovimentacao == eLAB_MovRestrito)
	{
		if (tpCelulaSeguinte == eLAB_Parede)
		{
			return LAB_CondRetEhParede;
		}

		MAT_IrParaNo(pLabirinto->pMatrizBase, direcao);

		if (tpCelulaSeguinte == eLAB_Saida)
		{
			return LAB_CondRetFimLabirinto;
		}

		else
		{
			return LAB_CondRetOK;
		}
	}

	/* Caso seja movimentação livre (pode-se passar por paredes, para facilitar a inserção das mesmas) */
	matCondRet = MAT_IrParaNo(pLabirinto->pMatrizBase, direcao);

	if (matCondRet == MAT_CondRetNaoPossuiAdjacencia)
	{
		return LAB_CondRetMovimentacaoInvalida;
	}

	return LAB_CondRetOK;
}

/***************************************************************************
*
*  Função: LAB Excluir Labirinto
*
***************************************************************************/

LAB_tpCondRet LAB_ExcluiLabirinto(LAB_tpLabirinto *pLabirinto)
{

	MAT_tpCondRet matCondRet;
	LAB_tpCondRet labCondRet;
	int tamMatriz;

	/* Verifica se o labirinto e grid existem (não são nulos) */
	labCondRet = !pLabirinto ? LAB_CondRetLabirintoNaoExiste : !pLabirinto->pMatrizBase ? LAB_CondRetLabirintoNaoExiste : LAB_CondRetOK;

	if (labCondRet != LAB_CondRetOK)
	{
		return labCondRet;
	}

	if (pLabirinto->LAB_colunas >= pLabirinto->LAB_linhas)
	{
		tamMatriz = pLabirinto->LAB_colunas;
	}

	else
	{
		tamMatriz = pLabirinto->LAB_linhas;
	}

	matCondRet = MAT_EsvaziaMatriz(pLabirinto->pMatrizBase, tamMatriz);

	switch (matCondRet)
	{
	case MAT_CondRetOK:
		return LAB_CondRetOK;
		break;

	case MAT_CondRetMatrizVazia:
		return LAB_CondRetLabirintoNaoExiste;
		break;
	}

	matCondRet = MAT_DestroiMatriz(pLabirinto->pMatrizBase, tamMatriz);
	free(pLabirinto);
	pLabirinto = NULL;

	return LAB_CondRetOK;
}

/* Código das funções encapsuladas no módulo */

/***********************************************************************
*
*  $FC Fun��o: LAB Verificar Célula
*
*  $FV Valor retornado
*     Inteiro indicando o tipo do nó para o qual deseja-se
*	  realizar o movimento.
*
***********************************************************************/

eLAB_tpCelula LAB_VerificaCelula(LAB_tpLabirinto *pLabirinto, int direcao)
{
	MAT_tpCondRet matCondRet;
	LAB_tpCelulaLabirinto **pValor;
	LAB_tpCelulaLabirinto *pAux2;
	eLAB_tpMovimentacao movBack;

	pAux2 = (LAB_tpCelulaLabirinto *)malloc(sizeof(LAB_tpCelulaLabirinto));
	pValor = (LAB_tpCelulaLabirinto **)malloc(sizeof(LAB_tpCelulaLabirinto));

	if (!pValor || !pAux2)
	{
		return eLAB_Invalido;
	}

	if (direcao != eLAB_Corrente)
	{

		/* Ida */
		matCondRet = MAT_IrParaNo(pLabirinto->pMatrizBase, direcao);

		if (matCondRet == MAT_CondRetNaoPossuiAdjacencia)
		{
			return eLAB_Parede;
		}

		/* Obtém o tipo do nó do grid. */
		matCondRet = MAT_ObterValorCorr(pLabirinto->pMatrizBase, &pValor);

		switch (direcao)
		{
		case eLAB_direita:
			movBack = eLAB_esquerda;
			break;
		case eLAB_cima:
			movBack = eLAB_baixo;
			break;
		case eLAB_baixo:
			movBack = eLAB_cima;
			break;
		case eLAB_esquerda:
			movBack = eLAB_direita;
			break;
		}

		/* Volta */
		MAT_IrParaNo(pLabirinto->pMatrizBase, movBack);
	}
	else
	{
		matCondRet = MAT_ObterValorCorr(pLabirinto->pMatrizBase, pValor);
	}

	if (pValor)
	{
		pAux2 = *pValor;
	}

	if (matCondRet == MAT_CondRetValNoNull)
	{
		return eLAB_Parede;
	}

	return pAux2->tpCelula;
}

/***********************************************************************
*
*  $FC Fun��o: Labirinto Default - LAB Criar Labirinto (auxiliar)
*
*  $FV Valor retornado
*     Sem retorno. Função auxiliar na 
*	  criação de um labirinto.
*
***********************************************************************/

void LabirintoDefault(LAB_tpLabirinto *pLabirinto, MAT_tppMatriz pMatriz, int xTamanho, int yTamanho)
{
	pLabirinto->pMatrizBase = pMatriz;
	pLabirinto->LAB_colunas = yTamanho;
	pLabirinto->LAB_linhas = xTamanho;
}
