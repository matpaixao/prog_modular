﻿#if !defined(LABIRINTO_)
#define LABIRINTO_

/**************************************************************
*
*   M�dulo de Defini��o: M�dulo Matriz
*
*   Autores: glg - Guilherme de Lacerda Gomes
*		 mrp - Matheus Romero Paixão
*                  fac - Fernanda de Almeida Castro
*
*   Hist�rico de evolu��o:
*	Legenda :
*		HotFixes, Atualizações e enums 		: Versão += 0.0.1 
*		Adições e/ou remoções de funções 	: Versão += 0.1.0
*		Finalização do Trabalho 			: Versão += 1.0.0
*
*   Vers�o      Autor       Data        Observa��es
*   1.0.0        glg		18/09/2019  Inicio de desenvolvimento.
*	2.0.0		 glg		26/09/2019  Refatora��o completa do m�dulo labrinto.
*	2.0.2		 mrp		27/09/2019	Reorganização elaborada(+ 0.0.2)
*	2.0.3		 mrp		30/09/2019	Atualizações do código 
*	2.1.0		 glg		30/09/2019  Atualização dos tipos e descrições das funções.
*	2.1.1		 mrp		01/10/2019  Hotfixes 
*	2.2.2		 glg		01/10/2019  Revisão de código
*   2.0.1        fac        03/10/2019  Atualiazação da documentação
*   2.0.2        fac        05/10/2019  Assertivas
*	2.1.0		 glg		06/10/2019  Correção das condições de retorno
*
*   Descri��o do M�dulo: 
*		Esse módulo visa implementar funções de criar, destruir, modificar,
*		mostrar e navegar por um labirinto .
*			Cada Labirinto possui uma estrutura "cabeça", no qual possui referência para 
*		sua grid (uma Matriz), também tem referencia para o tamanho máximo do labirinto 
*		definido pelo usuário, possui referencia para as coordenadas da entrada e saida 
*		do Labirinto. 
*
***************************************************************/

#if defined(LABIRINTO_OWN)
#define LABIRINTO_EXT
#else
#define LABIRINTO_EXT extern
#endif

typedef struct LAB_tpLabirinto *LAB_tppLabirinto;

/*************************************************************
 * 
 * 			Enum : Condicoes de retorno 
 * 			
 * 			Descrição : Definicao dos Valores de retorno
 * 						do modulo de Labirinto 
 * 
 **************************************************************/

typedef enum
{
	/* Ação executada corretamente */
	LAB_CondRetOK = 0,

	/* Tipo de Movimentação Inválida  */
	LAB_CondRetMovimentacaoInvalida = 1,

	/* Labirinto não foi criado */
	LAB_CondRetLabirintoNaoExiste = 2,

	/* Faltou memória */
	LAB_CondRetFaltouMemoria = 3,

	/* O usuário chegou ao final do labirinto */
	LAB_CondRetFimLabirinto = 4,

	/* O usuário tentou se locomover por uma parede */
	LAB_CondRetEhParede = 5,

	/* Erro genérico na ação */
	LAB_CondRetErroNaAcao = 7,

	/* Ação não permitida (ex: tentar modificar o mesmo nó para ser saída e entrada) */
	LAB_CondRetAcaoInvalida = 8,

	/* Tamanho do Labirinto Inválido */
	LAB_CondRetTamanhoInvalido = 9

} LAB_tpCondRet;

/*************************************************************
*
*   Fun��o: LAB Criar Labirinto
*
*	Par�metros
*		xTamanho - Inteiro contendo o tamanho do
*				   eixo vertical do labirinto.
*		yTamanho - Inteiro contendo o tamanho do
*				   eixo horizontal do labirinto.
*
*   Descri��o: Cria uma matriz que servir� como "grid" para
*			   a movimenta��o. A matriz � criada conforme o 
*			   maior tamanho passado como par�metro.
*
*   Valor Retornado:
*       - LAB_CondRetOK
*       - LAB_CondRetFaltouMemoria
*       - LAB_CondRetTamanhoInvalido
*
*      Assertivas:
*      AE - Ponteiro para uma labirinto
*            -Inteiro com numero de linhas do labirinto a ser criado
*           -Inteiro com numero de colunas do labirinto a ser criado
*      AS - Labirinto criado
*
**************************************************************/

LAB_tpCondRet LAB_CriarLabirinto(LAB_tppLabirinto pLabirinto, int xTamanho, int yTamanho);

/*************************************************************
*
*   Função: LAB Modificar Grid
*
*	Parâmetros
*		tipoModificacao - Inteiro contendo o tipo de modificação
*						  que será realizada.
*
*   Descrição: Modifica a posição atual do grid para:
*				Parede = 0,
*				Entrada = 1,
*				Saida = 2,
*				Caminho = 3,
*				Invalido = 4
*
*   Valor Retornado:
*       - LAB_CondRetOK
*       - LAB_CondRetFaltouMemoria
*	- LAB_CondRetLabirintoNaoExiste
*	- LAB_CondRetErroNaAcao
*
*      Assertivas:
*      AE - Ponteiro para uma labirinto
*            -Inteiro com o tipo de mudança do nó
*      AS - Nó corrente do labirinto com o tipo desejado (saída, entrada, parede ou caminho)
*
**************************************************************/

LAB_tpCondRet LAB_ModificarGrid(LAB_tppLabirinto pLabirinto, int tipoModificacao);

/*************************************************************
*
*   Função: LAB Andar pelo Lab
*
*	Parâmetros
*		direcao			 - Inteiro contendo a direção que deseja-se locomover.
			Corrente = -1,
			Cima = 5,
			Baixo = 6,
			Direita = 8,
			Esquerda = 7
*
*		tipoMovimentacao - Inteiro contendo o tipo de movimentação:
*			Livre	 - 0
*			Restrito - 1
*
*   Descrição: Realiza a locomoção pelo labirinto.
*
*   Valor Retornado:
*       - LAB_CondRetOK
*       - LAB_CondRetFaltouMemoria
*	- LAB_CondRetFimLabirinto
*	- LAB_CondRetEhParede
*	- LAB_CondRetLabirintoNaoExiste
*       -LAB_CondRetAcaoInvalida
*
*      Assertivas:
*      AE - Ponteiro para uma labirinto
*            -Inteiro com o tipo de direção que deseja-se locomover
*            -Inteiro com o tipo de movimentação
*      AS - Nó corrente do labirinto é o nó depois de feito a movimentação desejada
*
**************************************************************/

LAB_tpCondRet LAB_AndarPeloLab(LAB_tppLabirinto pLabirinto, int direcao, int tipoMovimentacao);

/*************************************************************
*
*   Função: LAB Excluir Labirinto
*
*	Parâmetros
*		pLabirinto - Ponteiro que referencia o Labirinto a ser excluido
*
*   Descrição: Realiza a exclusão do labirinto.
*
*   Valor Retornado:
*       - LAB_CondRetOK
*		- LAB_CondRetLabirintoNaoExiste
*
*      Assertivas:
*      AE - Ponteiro para uma labirinto
*      AS - Labirinto excluido
*		
**************************************************************/

LAB_tpCondRet LAB_ExcluiLabirinto(LAB_tppLabirinto pLabirinto);

#undef LABIRINTO_EXT

/************** Fim do m�dulo definido: M�dulo Labirinto. **************/

#else
#endif
