/***************************************************************************
*  $MCI M�dulo de implementa��o: M�dulo de teste espec�fico
*
*  Arquivo gerado:              testmat.c
*  Letras identificadoras:      TMAT
*
*  Projeto: Disciplina INF1301
*  Autor: glg - Guilherme de Lacerda Gomes
*
*  $HA Hist�rico de evolu��o:
*     Vers�o   Autor    Data         Observa��es
*       2.0.0   glg     07/09/2019   Corre��o de erros e implementa��o de mais testes.
*       3.0.0   glg     08/09/2019   T�rmino do desenvolvimento.
*		4.0.0	glg		09/09/2019	 Ajustes finais.
*		5.0.0   glg     07/10/2019   Adapta��o para teste sem lista.
*
*
***************************************************************************/

#include<string.h>
#include<stdio.h>
#include<malloc.h>

#include"tst_espc.h"
#include"lerparm.h"
#include"generico.h"
#include"matriz.h"

#define CRIAR_MAT_CMD "=criar"
#define INS_CORR_CMD "=insere"
#define IR_NO_CMD "=ir"
#define OBTER_VAL_CMD "=obter"
#define ESVAZIA_MAT_CMD "=esvazia"
#define DESTROI_MAT_CMD "=destroi"

#define DIM_VALOR_STR 7

MAT_tppMatriz *pMatriz;

/***********************************************************************
*
*  $FC Fun��o: TMAT &Testar matriz
*
*  $ED Descri��o da fun��o
*     Podem ser criadas at� 10 matrizes, identificadas pelos �ndices 0 a 9.
*
*     Comandos dispon�veis:
*
*     =criar							tamanho			CondRetEsp
*     =ir			                    direcao			CondRetEsp
*     =destroi							tamanho			CondRetEsp
*     =esvazia							tamanho			CondRetEsp
*     =inserir							indiceLista		CondRetEsp
*     =obter											CondRetEsp
*
***********************************************************************/


TST_tpCondRet TST_EfetuarComando(char * ComandoTeste)
{	
	int numLidos = -1;
	int indiceMatriz = -1;
	int indiceLista = -1;

	/* Varios casos necessitam do tamanho da matriz. */
	int tamMat = -1;

	/* Dire��o de movimenta��o na matriz. */
	int dir = -1;

	char val[DIM_VALOR_STR];
	char* pDado;

	/* Inicializa��o das condi��es de retorno. */
	MAT_tpCondRet CondRetEsperado = MAT_CondRetOK;
	MAT_tpCondRet CondRetObtido = MAT_CondRetFaltouMemoria;



	MAT_tppMatriz* pMat = (MAT_tppMatriz*)malloc(sizeof(MAT_tppMatriz*));

	/* Criar Matriz */
	if (strcmp(ComandoTeste, CRIAR_MAT_CMD) == 0)
	{
		numLidos = LER_LerParametros("ii", &tamMat, &CondRetEsperado);
		
		if (numLidos != 2)
		{
			return TST_CondRetParm;
		}

		CondRetObtido = MAT_CriarMatriz(pMat, tamMat);

		pMatriz = pMat;

		return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao criar a matriz.");
	}

	/* Ir para n�  */
	else if (strcmp(ComandoTeste, IR_NO_CMD) == 0)
	{
		numLidos = LER_LerParametros("ii", &dir, &CondRetEsperado);
		
		if (numLidos != 2)
		{
			return TST_CondRetParm;
		}

		CondRetObtido = MAT_IrParaNo(pMatriz, dir);
		
		return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao deslocar na matriz.");
	}

	/* Inserir no n� corrente */
	else if (strcmp(ComandoTeste, INS_CORR_CMD) == 0)
	{
		numLidos = LER_LerParametros("ii", val, &CondRetEsperado);

		if (numLidos != 2)
		{
			return TST_CondRetParm;
		}

		CondRetObtido = MAT_InsereValor(pMatriz, val);

		return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao inserir no no corrente.");
	}

	/* Obter valor do n� corrente */
	else if (strcmp(ComandoTeste, OBTER_VAL_CMD) == 0)
	{
		numLidos = LER_LerParametros("i", &CondRetEsperado);

		if (numLidos != 1)
		{
			return TST_CondRetParm;
		}

		CondRetObtido = MAT_ObterValorCorr(pMatriz, (void**)&pDado);
		
		return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao obter o valor corrente.");
	}

	/* Esvaziar a Matriz */
	else if (strcmp(ComandoTeste, ESVAZIA_MAT_CMD) == 0)
	{
		numLidos = LER_LerParametros("ii", &tamMat, &CondRetEsperado);

		if (numLidos != 2)
		{
			return TST_CondRetParm;
		}

		CondRetObtido = MAT_EsvaziaMatriz(pMatriz, tamMat);

		return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao esvaziar a matriz.");
	}

	/* Destruir a Matriz */
	else if (strcmp(ComandoTeste, DESTROI_MAT_CMD) == 0)
	{
		numLidos = LER_LerParametros("ii", &tamMat, &CondRetEsperado);

		if (numLidos != 2)
		{
			return TST_CondRetParm;
		}

		CondRetObtido = MAT_DestroiMatriz(pMatriz, tamMat);
		pMatriz = NULL;

		return TST_CompararInt(CondRetEsperado, CondRetObtido, "Erro ao destruir a matriz.");
	}

		return TST_CondRetNaoConhec;
	
	



}