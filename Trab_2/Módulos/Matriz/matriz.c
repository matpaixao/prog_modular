﻿/***************************************************************************
*  $MCI M�dulo de implementa��o: M�dulo matriz
*
*  Arquivo gerado:              MATRIZ.C
*  Letras identificadoras:      MAT
*
*  Projeto: Disciplina INF1301
*  Gestor:  DI/PUC-Rio
*  Autores: glg - Gulherme de Lacerda Gomes
*           mrp - Matheus Romero Paixão
*           fac - Fernanda de Almeida Castro
*
*  $HA Histórico de evolução:	
*	Legenda :
*		HotFixes, Atualizações e enums 		: Versão += 0.0.1 
*		Adições e/ou remoções de funções 	: Versão += 0.1.0
*		Finalização do Trabalho 			: Versão += 1.0.0
*
*     Versão  Autor    Data     Observações
*		1.0.1    glg  31/08/2019 Início do desenvolvimento
*		1.1.0    glg  01/09/2019 Adi��o de Fun��es Auxiliares e corre��o de erros.
*		1.1.1    glg  02/09/2019 --
*		1.1.2    glg  03/09/2019 Corre��o de Bugs e in�cio do desenvolvimento de novas fun��es do m�dulo de interface.
*		1.2.0    glg  03/09/2019 Adi��o de mais fun��es do m�dulo de interface.
*       2.0.0    glg  04/09/2019 Finaliza��o da implementa��o das fun��es do m�dulo de interface e testes unit�rios do m�dulo MATRIZ.c.
*		2.1.0    glg  07/09/2019 Corre��es finais.
*		2.1.1    glg  08/09/2019 Corre��o de erros na fun��o DestruirMatriz.
*		2.2.0	 glg  09/09/2019 Padroniza��o dos nomes das vari�veis.
*		2.3.0	 glg  18/09/2019 Modifica��o do tipo noMatriz para a aplica��o do labirinto.
*		2.3.1	 mrp  22/09/2019 Modificações no tipo Matriz e adição de novos enums 
*		2.4.0	 glg  22/09/2019 Adi��o da fun��o ModificarTipoNo e modifica��es no tpNoMatriz. Modifica��es na fun��o de locomo��o pela matriz.
*		2.4.1	 mrp  23/09/2019 Reorganiza funções, adiciona regions e enums
*                2.5.0   fac  23/09/2019 remoção de funções inúteis
*		2.5.1	 mrp  25/09/2019 Adiciona Enum e Atualiza função 
*		------------------ Refatoração ( - 0.3.0 ) ------------------
*		2.2.1    mrp  26/09/2019 Recolocação e reorganização de Módulos
*		2.3.0	 glg  27/09/2019 Conversão de funções e Organização Elaborada 
*		2.3.1	 mrp  01/10/2019 Hotfixes 
*		2.4.0    glg  02/10/2019 Correção da função obter valor.
* 
***************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#define MATRIZ_OWN
#include "matriz.h"
#undef MATRIZ_OWN

/***********************************************************************
*
*  $TC Tipo de dados: MAT Descritor da posi��o do n�
*
*
*  $ED Descri��o do tipo
*     Estas condi��es foram criadas para auxiliar na identifica��o da
*	  posi��o do n�, para facilidade a liga��o com os n�s adjacentes.
*
***********************************************************************/

typedef enum
{

	/* O n� encontra-se no canto superior esquerdo. */
	POS_CantoSupEsq = 1,

	/* O n� encontra-se no canto superior direito. */
	POS_CantoSupDir = 2,

	/* O n� encontra-se no canto inferior esquerdo.*/
	POS_CantoInfEsq = 3,

	/* O n� encontra-se no canto inferior direito. */
	POS_CantoInfDir = 4,

	/* O n� encontra-se na extremidade superior (linha mais a cima da matriz). */
	POS_ExtremidadeSup = 5,

	/* O n� encontra-se na extremidade inferior (linha mais a baixo da matriz). */
	POS_ExtremidadeInf = 6,

	/* O n� encontra-se na extremidade esquerda (linha mais a esquerda da matriz). */
	POS_ExtremidadeEsq = 7,

	/* O n� encontra-se na extremidade direita (linha mais a direita da matriz). */
	POS_ExtremidadeDir = 8,

	/* O n� encontra-se em uma posi��o gen�rica (possui oito liga��es). */
	POS_Generico = 9

} ePOS_posNo;

/***********************************************************************
*
*  $TC Tipo de dados: MAT Descritor do n� de uma matriz.
*
*
*  $ED Descri��o do tipo
*     O tipo refere-se ao n� de uma matriz. Cont�m ponteiros para os n�s adjacentes
*	  (caso n�o haja determinado n� adjacente, o ponteiro aponta para NULL) e para o
*	  valor inserido no n�. Possui, tamb�m, inteiros indicando a linha e a coluna do n�.
*
***********************************************************************/

typedef struct tpNoMatriz
{

	/* Ponteiro que aponta para o n� superior
	 *  Caso nao exista um n� superior, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoCima;

	/* Ponteiro que aponta para o n� inferior
	 *  Caso nao exista um n� inferior, o ponteiro
	 *  aponta para NULL. **/
	struct tpNoMatriz *pNoBaixo;

	/* Ponteiro que aponta para o n� a direita
	 *  Caso nao exista um n� a direita, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoDir;

	/* Ponteiro que aponta para o n� a esquerda
	 *  Caso nao exista um n� a esquerda, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoEsq;

	/* Ponteiro que aponta para o n� a noroeste
	 *  Caso nao exista um n� a noroeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoCimaEsq;

	/* Ponteiro que aponta para o n� a nordeste
	 *  Caso nao exista um n� a nordeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoCimaDir;

	/* Ponteiro que aponta para o n� a sudoeste
	 *  Caso nao exista um n� a sudoeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoBaixoEsq;

	/* Ponteiro que aponta para o n� a sudeste
	 *  Caso nao exista um n� a sudeste, o ponteiro
	 *  aponta para NULL. */
	struct tpNoMatriz *pNoBaixoDir;

	/* Ponteiro que aponta para o valor corrente
	* do n�. */
	void *pValor;

	/* Inteiro contendo a posi��o relativa a linha
	* do n�. */
	int linha;

	/* Inteiro contendo a posi��o relativa a coluna
	* do n�. */
	int coluna;

} tpNoMatriz;

/***********************************************************************
*
*  $TC Tipo de dados: MAT Descritor da cabe�a de uma matriz
*
*
*  $ED Descri��o do tipo
*     O tipo refere-se � cabe�a de uma matriz. Cont�m um ponteiro para o n�
*	  principal (1, 1), e um ponteiro para o n� corrente. Com isso, pode-se
*     locomover pela matriz facilmente.
*
***********************************************************************/

typedef struct MAT_tagMatriz
{
	/* Ponteiro que aponta para o n� "cabe�a", que
	*  no caso consideramos o n� (1,1). */
	tpNoMatriz *pNoCabeca;

	/* Ponteiro que aponta para o n� corrente. */
	tpNoMatriz *pNoCorrente;

} MAT_tpMatriz;

/* Protótipos das funções encapsuladas no módulo */

static tpNoMatriz *CriaNo(int linha, int coluna);

static MAT_tpCondRet LigaNos(tpNoMatriz **listaNos, int tam);

static ePOS_posNo EncontraPosicaoNo(tpNoMatriz tpNo, int tam);

static tpNoMatriz *ProcuraNo(tpNoMatriz **listaNo, int linha, int coluna, int tam);

/***************************************************************************
*
*  Função: MAT Criar Matriz
*
***************************************************************************/

MAT_tpCondRet MAT_CriarMatriz(MAT_tpMatriz *pMatriz, int tam)
{
	int linha = 1;
	int coluna = 1;
	int indice = 0;
	int step = 1;
	int counter = 0;
	MAT_tpCondRet ret;
	tpNoMatriz *pNoCorr;

	/* Vetor auxiliar para armazenar os n�s. */
	tpNoMatriz **pListaNo;

	/* Se tamanho <= 0 então o tamanho é inválido*/
	if (tam <= 0)
		return MAT_CondRetTamanhoInvalido;

	pListaNo = malloc(sizeof(tpNoMatriz *) * (tam * tam));
	if (pListaNo == NULL)
		return MAT_CondRetFaltouMemoria;

	/* Inser��o dos n�s no vetor, de acordo com o tamanho passado como par�metro. */
	for (counter = 0; counter <= (tam * tam) - 1; counter++)
	{
		if (counter % tam == 0 && counter != 0)
		{
			linha++;
			coluna = 1;
		}

		pNoCorr = CriaNo(linha, coluna);

		if (pNoCorr == NULL)
		{
			return MAT_CondRetFaltouMemoria;
		}

		if (tam == 1)
		{
			pMatriz->pNoCabeca = pNoCorr;
			pMatriz->pNoCorrente = pNoCorr;
			return MAT_CondRetOK;
		}

		if (step == 1)
		{
			pMatriz->pNoCabeca = pNoCorr;
			pMatriz->pNoCorrente = pNoCorr;
		}

		pListaNo[indice] = malloc(sizeof(tpNoMatriz));
		if (pListaNo[indice] == NULL)
		{
			return MAT_CondRetFaltouMemoria;
		}
		pListaNo[indice] = pNoCorr;

		indice++;
		step++;
		coluna++;
	}

	/* Liga��o dos n�s com seus adjacentes. */
	ret = LigaNos(pListaNo, tam);

	return ret;
}

/***************************************************************************
*
*  Função: MAT Ir Para Nó
*
***************************************************************************/

MAT_tpCondRet MAT_IrParaNo(MAT_tpMatriz *pMatriz, int dir)
{
	/* Matriz Nula */
	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	/* Verifica dire��o desejada. Primeiro, verifica-se se h� um n� em tal dire��o. */
	switch (dir)
	{
	case 1:
		if (pMatriz->pNoCorrente->pNoCimaEsq == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoCimaEsq;
		break;

	case 2:
		if (pMatriz->pNoCorrente->pNoCimaDir == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoCimaDir;
		break;

	case 3:
		if (pMatriz->pNoCorrente->pNoBaixoEsq == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixoEsq;
		break;

	case 4:
		if (pMatriz->pNoCorrente->pNoBaixoDir == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixoDir;
		break;

	case 5:
		if (pMatriz->pNoCorrente->pNoCima == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoCima;
		break;

	case 6:
		if (pMatriz->pNoCorrente->pNoBaixo == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixo;
		break;

	case 7:
		if (pMatriz->pNoCorrente->pNoEsq == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoEsq;
		break;

	case 8:
		if (pMatriz->pNoCorrente->pNoDir == NULL)
		{
			return MAT_CondRetNaoPossuiAdjacencia;
		}
		pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoDir;
		break;
	}

	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Função: MAT Destruir Matriz
*
***************************************************************************/

MAT_tpCondRet MAT_DestroiMatriz(MAT_tpMatriz *pMatriz, int tam)
{
	/* Vari�vel auxiliar criada para indicar
	 * qual dire��o deve-se locomover para
	 * continuar deletando (free) os n�s.
	 */
	int direcao = 1;

	int j = -1;
	int i = -1;
	tpNoMatriz *pProx = pMatriz->pNoCabeca;
	pMatriz->pNoCorrente = pMatriz->pNoCabeca;

	if (tam == 0)
		return MAT_CondRetTamanhoInvalido;

	for (i = 0; i < tam; i++)
	{
		switch (direcao)
		{
		case 1:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pNoDir != NULL)
				{
					pProx = pMatriz->pNoCorrente->pNoDir;
				}
				else
				{
					pProx = pProx->pNoBaixo;
				}

				free(pMatriz->pNoCorrente);

				/* Caso ja tenha varrido a matriz inteira, o ultimo no sera NULL */
				if (pProx != NULL)
				{
					pMatriz->pNoCorrente = pProx;
				}
			}
			direcao = 2;
			break;

		case 2:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pNoEsq != NULL)
				{
					pProx = pMatriz->pNoCorrente->pNoEsq;
				}
				else
				{
					pProx = pProx->pNoBaixo;
				}

				free(pMatriz->pNoCorrente);

				/* Caso ja tenha varrido a matriz inteira, o ultimo no sera NULL */
				if (pProx != NULL)
				{
					pMatriz->pNoCorrente = pProx;
				}
			}
			direcao = 1;
			break;
		}
	}

	free(pMatriz);

	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Função: MAT Obter Valor do Nó Corrente
*
***************************************************************************/

MAT_tpCondRet MAT_ObterValorCorr(MAT_tpMatriz *pMatriz, void **pValor)
{
	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	if (pMatriz->pNoCorrente->pValor == NULL)
	{
		pValor = NULL;
		return MAT_CondRetValNoNull;
	}

	*pValor = &pMatriz->pNoCorrente->pValor;

	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Função: MAT Inserir Valor no Nó
*
***************************************************************************/

MAT_tpCondRet MAT_InsereValor(MAT_tpMatriz *pMatriz, void *pValor)
{
	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	if (!pValor)
	{
		pMatriz->pNoCorrente->pValor = NULL;
	}
	else
	{
		pMatriz->pNoCorrente->pValor = pValor;
	}

	return MAT_CondRetOK;
}

/***************************************************************************
*
*  Função: MAT Esvaziar Matriz
*
***************************************************************************/

MAT_tpCondRet MAT_EsvaziaMatriz(MAT_tpMatriz *pMatriz, int tam)
{
	/* Vari�vel auxiliar criada para indicar
	 * qual dire��o deve-se locomover para
	 * continuar deletando (free) os n�s.
	 */
	int direcao = 1;

	int j = -1;
	int i = -1;
	int nosNull = 0;

	if (pMatriz->pNoCabeca == NULL)
	{
		return MAT_CondRetMatrizNaoExiste;
	}

	pMatriz->pNoCorrente = pMatriz->pNoCabeca;

	for (i = 0; i < tam; i++)
	{
		switch (direcao)
		{
		case 1:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pValor != NULL)
				{
					pMatriz->pNoCorrente->pValor = NULL;
				}
				else
				{
					nosNull++;
				}

				if (pMatriz->pNoCorrente->pNoDir != NULL)
				{
					pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoDir;
				}
			}
			direcao = 2;
			break;

		case 2:
			for (j = 0; j < tam; j++)
			{
				if (pMatriz->pNoCorrente->pValor != NULL)
				{
					pMatriz->pNoCorrente->pValor = NULL;
				}
				else
				{
					nosNull++;
				}

				if (pMatriz->pNoCorrente->pNoEsq != NULL)
				{
					pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoEsq;
				}
			}
			direcao = 1;
			break;
		}
		if (pMatriz->pNoCorrente->pNoBaixo != NULL)
		{
			pMatriz->pNoCorrente = pMatriz->pNoCorrente->pNoBaixo;
		}
	}

	if (nosNull == tam * tam)
	{
		pMatriz->pNoCorrente = pMatriz->pNoCabeca;
		return MAT_CondRetMatrizVazia;
	}

	/* Ao finalizar, definimos o n� corrente como sendo o n� cabe�a. */
	pMatriz->pNoCorrente = pMatriz->pNoCabeca;

	return MAT_CondRetOK;
}

/* Código das funções encapsuladas no módulo */

/***********************************************************************
*
*  $FC Fun��o: MAT Criar n� da Matriz
*
*  $FV Valor retornado
*     Ponteiro para o n� criado.
*     Ser� NULL caso a mem�ria tenha se esgotado.
*     Os ponteiros do n� criado estar�o nulos e o valor � igual ao do
*     par�metro.
*
***********************************************************************/

tpNoMatriz *CriaNo(int linha, int coluna)
{
	tpNoMatriz *pNoCorr;
	pNoCorr = (tpNoMatriz *)malloc(sizeof(tpNoMatriz));
	if (pNoCorr)
	{
		pNoCorr->coluna = coluna;
		pNoCorr->linha = linha;
		pNoCorr->pValor = NULL;
		pNoCorr->pNoBaixo = NULL;
		pNoCorr->pNoBaixoDir = NULL;
		pNoCorr->pNoBaixoEsq = NULL;
		pNoCorr->pNoCima = NULL;
		pNoCorr->pNoCimaDir = NULL;
		pNoCorr->pNoCimaEsq = NULL;
		pNoCorr->pNoDir = NULL;
		pNoCorr->pNoEsq = NULL;

		return pNoCorr;
	}
	return NULL;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Liga n�s da Matriz
*  $FV Valor retornado
*     Inteiro indicando se a liga��o dos n�s
*	  ocorreu sem problemas.
*
***********************************************************************/

MAT_tpCondRet LigaNos(tpNoMatriz **pListaNo, int tam)
{
	ePOS_posNo pos;
	int linha = -1;
	int coluna = -1;
	int tamAux = tam - 1;
	int tam2 = tam - tamAux;
	int i = 0;

	tpNoMatriz *pNoAdj = (tpNoMatriz *)malloc(sizeof(tpNoMatriz *));
	if (pNoAdj == NULL)
	{
		return MAT_CondRetFaltouMemoria;
	}

	/* Realiza a liga��o dos n�s baseado na posi��o dos mesmos. */
	for (i = 0; i < tam * tam; i++)
	{
		pos = EncontraPosicaoNo(*pListaNo[i], tam);

		switch (pos)
		{
		case (4):
			linha = tam;
			coluna = tam;

			/* Cima*/
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;
			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaEsq*/
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;
			break;
			/* Fim case 4 */

		case (3):
			linha = tam;
			coluna = tam2;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;
			break;
			/* Fim case 3 */

		/* Canto Sup Dir */
		case (2):
			linha = tam2;
			coluna = tam;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;
			break;
			/* Fim case 2 */

		/* Canto Sup Esq */
		case (1):
			linha = tam2;
			coluna = tam2;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;
			break;
			/* Fim case 1 */

		/* Extremimade Dir */
		case (8):
			linha = pListaNo[i]->linha;
			coluna = tam;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaEsq */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;
			break;
			/* Fim case 8 */

		/* Extremidade Esq */
		case (7):
			linha = pListaNo[i]->linha;
			coluna = tam2;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;
			break;
			/* Fim case 7 */

		/* Extremidade Inf */
		case (6):
			linha = tam;
			coluna = pListaNo[i]->coluna;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

			/* CimaEsq */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;
			break;
			/* Fim case 6 */

		/* Extremidade Sup */
		case (5):
			linha = tam2;
			coluna = pListaNo[i]->coluna;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;
			break;
			/* Fim case 5 */

		/* Generico */
		case (9):
			linha = pListaNo[i]->linha;
			coluna = pListaNo[i]->coluna;

			/* Cima */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna, tam);
			pListaNo[i]->pNoCima = pNoAdj;

			/* Baixo */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna, tam);
			pListaNo[i]->pNoBaixo = pNoAdj;

			/* Dir */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna + 1, tam);
			pListaNo[i]->pNoDir = pNoAdj;

			/* Esq */
			pNoAdj = ProcuraNo(pListaNo, linha, coluna - 1, tam);
			pListaNo[i]->pNoEsq = pNoAdj;

			/* CimaEsq */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna - 1, tam);
			pListaNo[i]->pNoCimaEsq = pNoAdj;

			/* CimaDir */
			pNoAdj = ProcuraNo(pListaNo, linha - 1, coluna + 1, tam);
			pListaNo[i]->pNoCimaDir = pNoAdj;

			/* BaixoEsq */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna - 1, tam);
			pListaNo[i]->pNoBaixoEsq = pNoAdj;

			/* BaixoDir */
			pNoAdj = ProcuraNo(pListaNo, linha + 1, coluna + 1, tam);
			pListaNo[i]->pNoBaixoDir = pNoAdj;
			break;
			/* Fim case 9 */
		}
	}

	return MAT_CondRetOK;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Encontrar Posi��o do N�
*
*  $FV Valor retornado
*     Inteiro referente � posi��o do n�.
*
***********************************************************************/

ePOS_posNo EncontraPosicaoNo(tpNoMatriz tpNo, int tam)
{
	int col;
	int lin;
	int tam2 = tam - 1;
	int tam3 = tam - tam2;
	col = tpNo.coluna;
	lin = tpNo.linha;

	if (lin == tam && col == tam3)
	{
		return POS_CantoInfEsq;
	}
	if (lin == tam3 && col == tam3)
	{
		return POS_CantoSupEsq;
	}
	if (lin == tam3 && col == tam)
	{
		return POS_CantoSupDir;
	}
	if (lin == tam && col == tam)
	{
		return POS_CantoInfDir;
	}
	if (lin == tam)
	{
		return POS_ExtremidadeInf;
	}
	if (lin == tam3)
	{
		return POS_ExtremidadeSup;
	}
	if (col == tam)
	{
		return POS_ExtremidadeDir;
	}
	if (col == tam3)
	{
		return POS_ExtremidadeEsq;
	}

	return POS_Generico;
}

/***********************************************************************
*
*  $FC Fun��o: MAT Acha n� adjacente

*  $FV Valor retornado
*     Ponteiro para o n� adjacente presente na lista de n�s.
*
***********************************************************************/

tpNoMatriz *ProcuraNo(tpNoMatriz **pListaNo, int linha, int coluna, int tam)
{
	int i = -1;

	for (i = 0; i < tam * tam; i++)
	{
		if (pListaNo[i]->coluna == coluna && pListaNo[i]->linha == linha)
		{
			return pListaNo[i];
		}
	}

	return NULL;
}
